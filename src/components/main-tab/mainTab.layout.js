import React, { Component } from 'react'
import {
  View,
  SafeAreaView,
  TouchableWithoutFeedback,
  StyleSheet
} from 'react-native'
import {
  Footer, FooterTab, Button, Icon, Text 
} from 'native-base'
import { Actions } from 'react-native-router-flux'

import { Metrics } from '../../ui/themes'

const activeTabStyle = 'rgb(164, 15, 16)'
const transparentColor = 'rgba(0, 0, 0, 0)'

const styles = StyleSheet.create({
  tab: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  tabIcon: {
    color: 'white',
    fontSize: 22 * Metrics.ratioScreen
  },
  tabText: {
    color: 'white',
    fontSize: 9 * Metrics.ratioScreen
  }
})

class Layout extends Component {
  componentWillMount() {
    // Actions.refresh()
    // alert(Actions.currentScene)
  }

  renderFooter() {
    const { activeTab } = this.props
    return (
      <Footer style={{ backgroundColor: 'rgba(0, 0, 0, 0)' }}>
        <FooterTab style={{ backgroundColor: 'rgb(28, 28, 28)' }}>
          <View style={{ flex: 1, backgroundColor: activeTab === 'game' ? activeTabStyle : transparentColor }}>
            <TouchableWithoutFeedback onPress={() => Actions.replace('game')}>
              <View style={styles.tab}>
                <Icon style={styles.tabIcon} name="cards-playing-outline" type="MaterialCommunityIcons" />
                <Text style={styles.tabText}>Game</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
          <View style={{ flex: 1, backgroundColor: activeTab === 'wallet' ? activeTabStyle : transparentColor }}>
            <TouchableWithoutFeedback onPress={() => Actions.replace('wallet')}>
              <View style={styles.tab}>
                <Icon style={styles.tabIcon} name="wallet" type="Entypo" />
                <Text style={styles.tabText}>Wallet</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
          <View style={{ flex: 1, backgroundColor: activeTab === 'betting' ? activeTabStyle : transparentColor }}>
            <TouchableWithoutFeedback onPress={() => Actions.replace('betting')}>
              <View style={styles.tab}>
                <Icon style={styles.tabIcon} name="ticket" type="MaterialCommunityIcons" />
                <Text style={styles.tabText}>Betting</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
          <View style={{ flex: 1, backgroundColor: activeTab === 'account' ? activeTabStyle : transparentColor }}>
            <TouchableWithoutFeedback onPress={() => Actions.replace('account')}>
              <View style={styles.tab}>
                <Icon style={styles.tabIcon} name="account-box" type="MaterialCommunityIcons" />
                <Text style={styles.tabText}>Betting</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </FooterTab>
      </Footer>
    )
  }

  render() {
    const { children } = this.props
    return (
      <View style={{ flex: 1 }}>
        <SafeAreaView style={{ flex: 1, backgroundColor: 'rgb(7 ,7 ,7)' }}>
          { children }
        </SafeAreaView>
        { this.renderFooter() }
      </View>
    )
  }
}

export default Layout
