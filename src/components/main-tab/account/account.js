import React, { Component } from 'react'
import {
  Image, StyleSheet, View 
} from 'react-native'
import {
  Button, Icon, Text
} from 'native-base'

import Header from '../../../ui/header'
import CommonStyle from '../mainTab.styles'

const styles = StyleSheet.create({
  HeaderContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
export default class Account extends Component {
  render() {
    const { translate } = this.props
    return (
      <View style={CommonStyle.MainScreenContainer}>
        <Header>
          <View style={styles.HeaderContainer}>
            <Text style={CommonStyle.HeaderText}>ACCOUNT</Text>
          </View>
        </Header>
      </View>
    )
  }
}
