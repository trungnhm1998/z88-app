import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Account from './account'
import Layout from '../mainTab.layout'

class AccountContainer extends Component {
  componentWillMount() {
  }

  render() {
    const { t: translate } = this.context
    return (
      <Layout activeTab="account">
        <Account translate={translate} />
      </Layout>
    )
  }
}

function mapStateToProps(state) {
  return {
    i18nState: state.i18nState
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AccountContainer)

AccountContainer.contextTypes = {
  t: PropTypes.func
}
