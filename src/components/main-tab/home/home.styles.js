import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  Logo: {
    height: '100%',
    width: '50%',
    resizeMode: 'contain'
  },
  SigninButton: {
    backgroundColor: 'rgb(51, 172, 66)',
    height: 32
  },
  gameMenuContainer: {
    height: 100,
    backgroundColor: 'rgb(34, 34, 34)',
    paddingLeft: 5
  },
  gameMenuScrollViewContainer: {
    alignItems: 'center'
  },
  mainContentContainer: {
    margin: 8
  },
  newGameAndSportBetContainer: {
    flex: 2,
    flexDirection: 'row'
  },
  newGame: {
    flex: 2,
    paddingRight: 4
  },
  sportbet: {
    flex: 2,
    paddingLeft: 4
  },
  promotionContainer: {
    flex: 2,
    paddingTop: 8
  }
})

export default styles
