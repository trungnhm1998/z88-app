import React, { Component } from 'react'
import {
  Image, StyleSheet, View, Dimensions,
  ScrollView, StatusBar, SafeAreaView
} from 'react-native'
import {
  Button, Icon, Text, Footer, FooterTab
} from 'native-base'
import FitImage from 'react-native-fit-image'
import { Actions } from 'react-native-router-flux'

import Header from '../../../ui/header'
import CommonStyle from '../mainTab.styles'
import { Themes } from '../../../ui'
import GameMenuButton from '../../../ui/game-menu-button'
import styles from './home.styles'

export default class Home extends Component {
  componentDidMount() {
  }

  renderHeader() {
    const { translate } = this.props
    return (
      <Header>
        <StatusBar
          barStyle="light-content"
        />
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{
            flex: 1, paddingBottom: 8, paddingTop: 8, paddingLeft: 8 
          }}
          >
            <Image style={styles.Logo} source={Themes.Images.headerLogo} />
          </View>
          <View style={{
            flex: 1,
            justifyContent: 'flex-end',
            flexDirection: 'row',
            alignItems: 'center',
            paddingRight: 16
          }}
          >
            <View>
              <Button style={styles.SigninButton} iconLeft light onPress={() => Actions.signin()}>
                <Icon type="FontAwesome" name="user" style={{ color: 'white' }} />
                <Text style={{ color: 'white' }}>{translate('Đăng nhập')}</Text>
              </Button>
            </View>
          </View>
        </View>
      </Header>
    )
  }

  renderBanner() {
    const { i18nState } = this.props
    return (
      <FitImage
        originalHeight={296}
        originalWidth={750}
        resizeMode="contain"
        source={i18nState.lang === 'en' ? Themes.Images.homeBannerEn : Themes.Images.homeBanner}
      />
    )
  }

  // TODO: Implement Action.[Scene] here
  onSportbookPress() {
    alert('sportbook')
  }

  // TODO: Implement Action.[Scene] here
  onLiveCasinoPress() {
    alert('live casino')
  }

  // TODO: Implement Action.[Scene] here
  onCasinoIMPress() {
    alert('Casino IM')
  }

  // TODO: Implement Action.[Scene] here
  onSlotPress() {
    alert('Slot')
  }

  renderGameMenu() {
    const { menu, translate } = this.props
    // const menu = [ {}, {}, {}, {}, {}, {}, {}, {} ]
    console.log(menu)
    const menuUI = menu.map((item) => {
      let onPress
      return (
        <GameMenuButton
          key={item.name}
          title={item.name}
          iconName="slot-machine-silhouette"
          onPress={onPress}
        />
      )
    })
    return (
      <View style={styles.gameMenuContainer}>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={styles.gameMenuScrollViewContainer}
        >
          { menuUI }
        </ScrollView>
      </View>
    )
  }

  renderMainContent() {
    const { i18nState } = this.props
    return (
      <View style={styles.mainContentContainer}>
        <View style={styles.newGameAndSportBetContainer}>
          <View style={styles.newGame}>
            <FitImage
              originalHeight={279}
              originalWidth={349}
              resizeMode="contain"
              source={i18nState.lang === 'en' ? Themes.Images.newGameEn : Themes.Images.newGameVi}
            />
          </View>
          <View style={styles.sportbet}>
            <FitImage
              originalHeight={279}
              originalWidth={349}
              resizeMode="contain"
              source={i18nState.lang === 'en' ? Themes.Images.sportBetEn : Themes.Images.sportBetVi}
            />
          </View>
        </View>
        <View style={styles.promotionContainer}>
          <FitImage
            originalHeight={230}
            originalWidth={707}
            resizeMode="contain"
            source={i18nState.lang === 'en' ? Themes.Images.homePromotionEn : Themes.Images.homePromotionVi}
          />
        </View>

      </View>
    )
  }

  renderFooter() {
    return (
      <Footer style={{ backgroundColor: 'green' }}>
        <FooterTab style={{ backgroundColor: 'black' }}>
          <View style={{ flex: 1, backgroundColor: 'red'}}>

          </View>
          <View style={{ flex: 1, backgroundColor: 'pink'}}>

          </View>
          <Button active>
            <Icon active name="navigate" />
          </Button>
          <Button>
            <Icon name="person" />
          </Button>
        </FooterTab>
      </Footer>
    )
  }

  render() {
    return (
      <View style={CommonStyle.MainScreenContainer}>
        { this.renderHeader() }
        <ScrollView>
          { this.renderBanner() }
          { this.renderGameMenu() }
          { this.renderMainContent() }
        </ScrollView>
      </View>
    )
  }
}
