import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Button, View } from 'react-native'
import sl from 'react-native-splash-screen'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as z88Actions from '../../../redux/z88/z88.actions'
import Home from './home'
import Layout from '../mainTab.layout'

class HomeContainer extends Component {
  componentWillMount() {
    sl.hide()
    // this.props.testActions.Increment()
    const { z88Action: { getMenuGame, getProductWallet } } = this.props
    getMenuGame()
  }

  render() {
    const { t: translate } = this.context
    const { menu, i18nState, router } = this.props
    return (
      <Layout activeTab="game">
        <Home i18nState={i18nState} menu={menu} translate={translate} />
      </Layout>
    )
  }
}

function mapStateToProps(state) {
  return {
    i18nState: state.i18nState,
    menu: state.z88.menu,
    router: state.router
  }
}

function mapDispatchToProps(dispatch) {
  return {
    z88Action: bindActionCreators(z88Actions, dispatch),
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer)

HomeContainer.contextTypes = {
  t: PropTypes.func
  // router: PropTypes.object
}
