import React, { Component } from 'react'
import {
  Image, StyleSheet, View 
} from 'react-native'
import {
  Button, Icon, Text
} from 'native-base'

import Header from '../../../ui/header'
import CommonStyle from '../mainTab.styles'

const styles = StyleSheet.create({
  HeaderContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  HeaderRightButton: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'flex-end'
  }
})
export default class Wallets extends Component {
  render() {
    const { translate } = this.props
    return (
      <View style={CommonStyle.MainScreenContainer}>
        <Header>
          <View style={styles.HeaderContainer}>
            <Text style={CommonStyle.HeaderText}>WALLETS</Text>
          </View>
          <View
            style={styles.HeaderRightButton}
          >
            <View style={{ paddingRight: 16 }}>
              <Icon style={{ color: 'gold' }} type="MaterialCommunityIcons" name="timetable" />
            </View>
          </View>
        </Header>
      </View>
    )
  }
}
