import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Wallets from './wallets'
import Layout from '../mainTab.layout'

class WalletsContainer extends Component {
  componentWillMount() {
  }

  render() {
    const { t: translate } = this.context
    return (
      <Layout activeTab="wallet">
        <Wallets translate={translate} />
      </Layout>
    )
  }
}

function mapStateToProps(state) {
  return {
    i18nState: state.i18nState
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WalletsContainer)

WalletsContainer.contextTypes = {
  t: PropTypes.func
}
