import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Betting from './betting'
import Layout from '../mainTab.layout'


class BettingContainer extends Component {
  componentWillMount() {
  }

  render() {
    const { t: translate } = this.context
    return (
      <Layout activeTab="betting">
        <Betting translate={translate} />
      </Layout>
    )
  }
}

function mapStateToProps(state) {
  return {
    i18nState: state.i18nState
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BettingContainer)

BettingContainer.contextTypes = {
  t: PropTypes.func
}
