import { StyleSheet } from 'react-native'
import { Themes } from '../../ui'

const styles = StyleSheet.create({
  MainScreenContainer: {
    flex: 1,
    backgroundColor: 'rgb(7, 7, 7)'
  },
  HeaderText: {
    color: 'white',
    fontSize: Themes.Fonts.size.regular
  }
})

export default styles
