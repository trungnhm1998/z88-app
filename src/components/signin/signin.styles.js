import { StyleSheet } from 'react-native'
import { Themes } from '../../ui'

const styles = StyleSheet.create({
  contentContainer: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    justifyContent: 'center'
  },
  logoContainer: {
    position: 'absolute',
    top: '5%',
    width: '100%',
    height: 80 * Themes.Metrics.ratioScreen
  },
  logo: {
    flex: 1,
    width: null,
    height: null
  },
  closeButton: {
    color: 'white',
    fontSize: 30 * Themes.Metrics.ratioScreen,
    fontWeight: 'bold',
    margin: 20 * Themes.Metrics.ratioScreen
  },
  marginHorizontal30: {
    marginHorizontal: 30
  },
  supportContainer: {
    width: '100%',
    height: 30 * Themes.Metrics.ratioScreen,
    flexDirection: 'row',
    marginTop: 15 * Themes.Metrics.ratioScreen
  },
  supportText: {
    flex: 2,
    justifyContent: 'flex-end'
  },
  textUnderline: {
    textDecorationLine: 'underline',
    color: 'white'
  },
  divider: {
    borderBottomColor: 'rgba(255, 255, 255, 0.2)',
    borderBottomWidth: 0.5,
    marginVertical: 10 * Themes.Metrics.ratioScreen
  },
  buttonContainer: {
    borderRadius: 3 * Themes.Metrics.ratioScreen
  },
  button: {
    shadowOffset: { height: 0, width: 0 },
    shadowOpacity: 0,
    elevation: 0,
    backgroundColor: 'rgba(0, 0, 0, 0)'
  }
})

export default styles
