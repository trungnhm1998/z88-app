import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'

import Signin from './signin'

class SigninContainer extends Component {
  componentWillMount() {
  }

  render() {
    const { t: translate } = this.context
    return (
      <Signin translate={translate} />
    )
  }
}

function mapStateToProps(state) {
  return {
    i18nState: state.i18nState
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SigninContainer)

SigninContainer.contextTypes = {
  t: PropTypes.func
}
