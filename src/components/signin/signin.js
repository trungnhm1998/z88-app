import React, { Component } from 'react'
import {
  Image, StyleSheet, View,
  TouchableWithoutFeedback,
  Keyboard
} from 'react-native'
import {
  Button, Icon, Text
} from 'native-base'
import { Actions } from 'react-native-router-flux'
import t from 'tcomb-form-native'
import LinearGradient from 'react-native-linear-gradient'

import { Themes, TcombForm } from '../../ui'
import styles from './signin.styles'

const Login = t.struct({
  email: t.String,
  password: t.String
})

export default class Account extends Component {
  constructor(props) {
    super(props)

    this.state = {
      logoHeight: styles.logoContainer.height
    }
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => this.keyboardDidShow())
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => this.keyboardDidHide())
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  keyboardDidShow() {
    this.setState({ logoHeight: 30 * Themes.Metrics.ratioScreen })
  }

  keyboardDidHide() {
    this.setState({ logoHeight: styles.logoContainer.height })
  }

  renderLogo() {
    const { logoHeight } = this.state
    return (
      <View style={[styles.logoContainer, { height: logoHeight }]}>
        <Image
          resizeMode="contain"
          style={styles.logo}
          source={Themes.Images.headerLogo}
        />
      </View>
    )
  }

  renderCloseButton() {
    return (
      <View style={{ position: 'absolute', width: '100%' }}>
        <View style={{ flex: 1, alignItems: 'flex-end' }}>
          <TouchableWithoutFeedback onPress={() => Actions.pop()}>
            <Icon
              style={styles.closeButton}
              type="MaterialCommunityIcons"
              name="window-close"
            />
          </TouchableWithoutFeedback>
        </View>
      </View>
    )
  }

  renderLoginForm() {
    const { Form } = t.form
    const { translate } = this.props
    const options = {
      stylesheet: TcombForm.styles,
      fields: {
        email: {
          template: TcombForm.InputTextNormal,
          autoFocus: true,
          config: {
            label: 'Email'
          }
        },
        password: {
          template: TcombForm.InputTextNormal,
          secureTextEntry: true,
          config: {
            label: 'Password'
          }
        }
      }
    }

    return (
      <View style={styles.marginHorizontal30}>
        <Form type={Login} options={options} />
        <LinearGradient colors={['rgb(254, 52, 4)', 'rgb(231, 50, 9)']} style={styles.buttonContainer}>
          <Button block style={styles.button}>
            <Text style={{ color: 'white' }}>{translate('ĐĂNG NHẬP')}</Text>
          </Button>
        </LinearGradient>
        <View style={styles.supportContainer}>
          <View style={{ ...styles.supportText, ...{ alignItems: 'flex-start' } }}>
            <Text style={styles.textUnderline}>
              {translate('Quên mật khẩu')}
            </Text>
          </View>

          <View style={{ ...styles.supportText, ...{ alignItems: 'flex-end' } }}>
            <Text style={styles.textUnderline}>
              {translate('Hỗ trợ trực tuyến')}
            </Text>
          </View>
        </View>
        <View style={styles.divider} />
        <LinearGradient colors={['rgb(35, 213, 55)', 'rgb(51, 172, 66)']} style={styles.buttonContainer}>
          <Button block style={styles.button} onPress={() => Actions.signup()}>
            <Text style={{ color: 'white' }}>{translate('ĐĂNG KÝ')}</Text>
          </Button>
        </LinearGradient>
      </View>
    )
  }

  render() {
    const { translate } = this.props
    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={{ flex: 1, backgroundColor: 'rgb(7, 7, 7)' }}>
          <Image
            style={{ flex: 1, width: null, height: null }}
            resizeMethod="resize"
            resizeMode="cover"
            source={Themes.Images.backgroundImage}
          />
          <View style={styles.contentContainer}>
            { this.renderLoginForm() }
          </View>
          { this.renderLogo() }
          { this.renderCloseButton() }
        </View>
      </TouchableWithoutFeedback>
    )
  }
}
