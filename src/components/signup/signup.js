import React, { Component } from 'react'
import {
  Image, StyleSheet, View,
  ScrollView,
  Keyboard,
  TouchableOpacity
} from 'react-native'
import { Actions } from 'react-native-router-flux'
import DateTimePicker from 'react-native-modal-datetime-picker'
import Picker from 'react-native-picker'
import {
  Icon, Text
} from 'native-base'
import t from 'tcomb-form-native'
import moment from 'moment'
import { Themes, TcombForm } from '../../ui'
import Header from '../../ui/header'

const Form = t.form.Form

const Gender = t.enums({
  M: 'Male',
  F: 'Female'
})

const RegisterStruct = t.struct({
  firstName: t.String,
  surName: t.String,
  birthday: t.Date,
  gender: Gender
})

export default class Signup extends Component {
  constructor(props) {
    super(props)

    this.showDateTimePicker = this.showDateTimePicker.bind(this)
    this.hideDateTimePicker = this.hideDateTimePicker.bind(this)
    this.handleDatePicked = this.handleDatePicked.bind(this)

    this.state = {
      isDateTimePickerVisible: false,
      value: {
        birthday: ''
      }
    }
  }

  showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true })

  hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false })

  handleDatePicked(date) {
    const birthday = moment(date).format('DD/MM/YYYY')
    this.setState({
      value: {
        birthday
      }
    })
    this.hideDateTimePicker()
  }


  renderForm() {
    const options = {
      stylesheet: TcombForm.styles,
      fields: {
        birthday: {
          template: TcombForm.InputRightIcon,
          placeholder: 'DD/MM/YY',
          placeholderTextColor: '#E7E7E7',
          config: {
            label: 'Ngày sinh',
            useVectorIcon: true,
            rightIconName: 'calendar',
            rightIconType: 'MaterialCommunityIcons',
            rightIconSize: 20,
            onFocus: () => {
              console.log('onFocus called')
              Picker.hide()
              Keyboard.dismiss()
              this.showDateTimePicker()
            }
          }
        }
      }
    }

    return (
      <Form 
        type={RegisterStruct}
        options={options}
        ref={(form) => {
          this.form = form
        }}
      />
    )
  }

  renderHeader() {
    const { translate } = this.props
    return (
      <Header>
        <View
          style={Themes.Styles.HeaderLeftButton}
        >
          <TouchableOpacity onPress={() => Actions.pop()}>
            <View style={{ paddingLeft: 16 }}>
              <Icon style={{ color: 'gold' }} type="MaterialCommunityIcons" name="keyboard-backspace" />
            </View>
          </TouchableOpacity>
        </View>
        <View style={Themes.Styles.HeaderContainer}>
          <Text style={{ color: Themes.Colors.headerText, fontSize: Themes.Fonts.size.regular }}>{translate('ĐĂNG KÝ')}</Text>
        </View>
      </Header>
    )
  }

  render() {
    console.log(this.state)
    return (
      <View style={{ flex: 1, backgroundColor: Themes.Colors.mainBackground }}>
        {this.renderHeader()}
        <ScrollView>
          {this.renderForm()}
          <DateTimePicker
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={this.handleDatePicked}
            onCancel={this.hideDateTimePicker}
            confirmTextIOS="Ok"
            date={moment(this.state.value.birthday ? this.state.value.birthday : '', 'DD/MM/YYYY').toDate()}  
          />
        </ScrollView>
      </View>
    )
  }
}
