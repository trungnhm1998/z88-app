import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'

import Signup from './signup'

class SignupContainer extends Component {
  componentWillMount() {
  }

  render() {
    const { t: translate } = this.context
    return (
      <Signup translate={translate} />
    )
  }
}

function mapStateToProps(state) {
  return {
    i18nState: state.i18nState
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignupContainer)

SignupContainer.contextTypes = {
  t: PropTypes.func
}
