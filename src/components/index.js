import Home from './main-tab/home/home.container'
import Wallet from './main-tab/wallet/wallets.container'
import Account from './main-tab/account/account.container'
import Betting from './main-tab/betting/betting.container'
import Signin from './signin/signin.container'
import Signup from './signup/signup.container'

export {
  Home,
  Wallet,
  Account,
  Betting,
  Signin,
  Signup
}
