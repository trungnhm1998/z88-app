import * as types from './api.types'

const initialState = {
  showLoading: false,
  showPanelMenu: false,
  openModal: false,
  checkLogin: false,
  namePage: '',
  type: null,
  data: {}
}

export default function api(state = initialState, action) {
  switch (action.type) {
    case types.SHOW_LOADING:
      return {
        ...initialState,
        showLoading: true
      }
    case types.HIDE_LOADING:
      return {
        ...initialState,
        showLoading: false
      }
    case types.CHECK_LOGIN:
      return {
        ...initialState,
        checkLogin: true
      }
    case types.UNCHECK_LOGIN:
      return {
        ...initialState,
        checkLogin: false
      }
    case types.SHOW_PANEL_MENU:
      return {
        ...initialState,
        showPanelMenu: true
      }
    case types.HIDE_PANEL_MENU:
      return {
        ...initialState,
        showPanelMenu: false
      }
    case types.OPEN_MODAL:
      return {
        ...initialState,
        openModal: true
      }
    case types.CLOSE_MODAL:
      return {
        ...initialState,
        openModal: false
      }
    case types.NAVIGATE_PAGE:
      return {
        ...initialState,
        namePage: action.payload
      }
    case types.STORE_API_MESSAGE: {
      const { type, message, code, isDefault } = action.payload
      return {
        // ...state,
        type,
        message,
        code,
        isDefault
      }
    }
    case types.STORE_API_PAYLOAD: {
      const { type, payload } = action.payload
      return {
        // ...state,
        type,
        data: { ...payload }
      }
    }
    case types.CLEAR_API_RESPONSE:
      return {}
    default:
      return state
  }
}

