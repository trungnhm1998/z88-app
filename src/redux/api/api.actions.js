import * as types from './api.types'

export function initApplication(response) {
  return {
    type: types.INIT_APPLICATION,
    payload: { ...response }
  }
}

export function storeResponseMessage(response) {
  return {
    type: types.STORE_API_MESSAGE,
    payload: { ...response }
  }
}

export function storeResponsePayload(response) {
  return {
    type: types.STORE_API_PAYLOAD,
    payload: { ...response }
  }
}

export function clearResponse() {
  return {
    type: types.CLEAR_API_RESPONSE
  }
}
