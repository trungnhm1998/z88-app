import * as types from './z88.types'

export function doDepositTransaction(payload) {
  return {
    type: types.DO_DEPOSIT_TRANSACTION,
    payload
  }
}
export function getDepositTransactionResult(payload) {
  return {
    type: types.GET_DEPOSIT_TRANSACTION_RESULT,
    payload
  }
}
export function getCurrentBalanceStatus(payload) {
  return {
    type: types.GET_CURRENT_BALANCE_STATUS,
    payload
  }
}
export function getHistory(payload) {
  return {
    type: types.GET_HISTORY,
    payload
  }
}
export function getTransferHistory(payload) {
  return {
    type: types.GET_TRANSFER_HISTORY,
    payload
  }
}
export function getGameLogs(payload) {
  return {
    type: types.GET_GAMES_LOGS,
    payload
  }
}
export function getPaymentMethods(payload) {
  return {
    type: types.GET_PAYMENT_METHODS,
    payload
  }
}
export function doTransferTransaction(payload) {
  const { fromAccount: srcWallet, toAccount: dstWallet, amount, currency } = payload
  const request = []
  const fromAccount = parseInt(srcWallet, 10)
  const toAccount = parseInt(dstWallet, 10)
  const withdraw = {
    transferType: 'withdraw',
    amount,
    currency,
    productWallet: fromAccount
  }
  const deposit = {
    transferType: 'deposit',
    amount,
    currency,
    productWallet: toAccount
  }
  if ((fromAccount * toAccount) !== 0) {
    request.push(withdraw, deposit)
  } else if (fromAccount === 0) {
    request.push(deposit)
  } else if (toAccount === 0) {
    request.push(withdraw)
  }

  return {
    type: types.DO_TRANSFER_TRANSACTION,
    payload: request
  }
}
export function getTransferTransactionStatus(payload) {
  return {
    type: types.GET_TRANSFER_TRANSACTION_STATUS,
    payload
  }
}
export function getInformationOfJackpot(payload) {
  return {
    type: types.GET_INFORMATION_OF_JACKPOT,
    payload
  }
}
export function getLogToLaunchGame(payload) {
  return {
    type: types.GET_LOG_TO_LAUNCH_GAME,
    payload
  }
}
export function launchTryItGame(payload) {
  return {
    type: types.LAUNCH_TRY_IT_GAME,
    payload
  }
}

export function getProfileOfUser(payload) {
  return {
    type: types.GET_PROFILE_OF_USER,
    payload
  }
}
export function changePassword(payload) {
  return {
    type: types.CHANGE_PASSWORD,
    payload
  }
}
export function login(payload) {
  return {
    type: types.LOGIN,
    payload
  }
}
export function logout(payload) {
  return {
    type: types.LOGOUT,
    payload
  }
}
export function registerAccountByEmail(payload) {
  return {
    type: types.REGISTER_ACCOUNT_BY_EMAIL,
    payload
  }
}
export function sendEmailToResetPasswordForgot(payload) {
  return {
    type: types.FORGOT_PASSWORD,
    payload
  }
}
export function resetPasswordForgot(payload) {
  return {
    type: types.RESET_PASSWORD_FORGOT,
    payload
  }
}
export function updateAvatarOfUser(payload) {
  return {
    type: types.UPDATE_AVATAR_OF_USER,
    payload
  }
}
export function updateProfileOfUser(payload) {
  return {
    type: types.UPDATE_PROFILE_OF_USER,
    payload
  }
}

export function getMenuGame(payload) {
  return {
    type: types.GET_MENU_GAME,
    payload
  }
}
export function checkAccessToken(payload) {
  return {
    type: types.CHECK_ACCESS_TOKEN,
    payload
  }
}
export function checkMissingProfile(payload) {
  return {
    type: types.CHECK_MISSING_PROFILE,
    payload
  }
}

export function getProductWallet(payload) {
  return {
    type: types.GET_PRODUCT_WALLET,
    payload
  }
}
export function getCaptcha(payload) {
  return {
    type: types.GET_CAPTCHA,
    payload
  }
}
export function startTransaction(payload) {
  return {
    type: types.START_TRANSACTION,
    payload
  }
}
export function endTransaction(payload) {
  return {
    type: types.END_TRANSACTION,
    payload
  }
}
export function clearRedirect(payload) {
  return {
    type: types.CLEAR_REDIRECT,
    payload
  }
}
export function missProfile(payload) {
  return {
    type: types.MISSING_PROFILE,
    payload
  }
}
export function startHandleError(payload) {
  return {
    type: types.START_HANDLE_ERROR,
    payload
  }
}
export function stopHandleError(payload) {
  return {
    type: types.STOP_HANDLE_ERROR,
    payload
  }
}
export function emitMessage(payload) {
  return {
    type: types.EMIT_MESSAGE,
    payload
  }
}

export function copyToClipBoard(payload) {
  return {
    type: types.COPY_TO_CLIPBOARD,
    payload
  }
}
export function clearClipBoard(payload) {
  return {
    type: types.CLEAR_CLIPBOARD,
    payload
  }
}
export function doWithdrawTransaction(payload) {
  return {
    type: types.DO_WITHDRAW_TRANSACTION,
    payload
  }
}
export function changeChannel(payload) {
  return {
    type: types.CHANGE_CHANNEL,
    payload
  }
}
export function clearCaptcha(payload) {
  return {
    type: types.CLEAR_CAPTCHA,
    payload
  }
}
export function clearLogs(payload) {
  return {
    type: types.CLEAR_LOGS,
    payload
  }
}
export function getCategoriesGame(payload) {
  return {
    type: types.GET_CATEGORIES_GAME,
    payload
  }
}
export function getGameByCategory(payload) {
  return {
    type: types.GET_GAME_BY_CATEGORY,
    payload
  }
}
export function getGameCasinoOnline(payload) {
  return {
    type: types.GET_GAME_CASINO_ONLINE,
    payload
  }
}

export function removeLogLaunch(payload) {
  return {
    type: types.REMOVE_LOG_LAUNCH,
    payload
  }
}

// get invite
export function getInviteTop20(payload) {
  return {
    type: types.GET_INVITE_TOP_20,
    payload
  }
}
export function pushAlert(payload) {
  return {
    type: types.EMIT_ALERT,
    payload
  }
}
export function getCountry(payload) {
  return {
    type: types.GET_COUNTRY,
    payload
  }
}
export function getCategoryPromotion(payload) {
  return {
    type: types.GET_CATEGORY_PROMOTION,
    payload
  }
}
export function getByCategoryPromotion(payload) {
  return {
    type: types.GET_BY_CATEGORY_PROMOTION,
    payload
  }
}
export function gameGetById(payload) {
  return {
    type: types.GAME_GET_BY_ID,
    payload
  }
}

export function sendOtpVerifyEmail(payload) {
  return {
    type: types.SEND_OTP_VERIFY_EMAIL,
    payload
  }
}

export function sendAuthenOtp(payload) {
  return {
    type: types.SEND_AUTHEN_OTP,
    payload
  }
}

/**
 * Lấy thông tin các trang giới thiệu, điều khoản,..
 */
export function getPageOfAlias(payload) {
  return {
    type: types.GET_PAGE_OF_ALIAS,
    payload
  }
}

/**
 * lấy thông tin payment btc 
 */
export function getBTCPaymentInfo() {
  return {
    type: types.GET_BTC_PAYMENT_INFO
  }
}
