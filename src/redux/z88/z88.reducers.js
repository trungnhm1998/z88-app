import * as types from './z88.types'
import * as responseCode from '../../configs/responseCode.config'

const { userAgent, appName: deviceId, languages: lang, appVersion } = navigator
const infoClient = {
  userAgent,
  platform: 'web',
  // lang: lang[0],
  // version: appVersion.split('')[0],
  channel: '68|ref39',
  deviceId
}
const MAIN_WALLET = 0
const initialState = {
  authCode: '',
  sportbookUrl: '',
  fishingGameUrl: '',
  captcha: {},
  dataSendOptVerify: {},
  authenticate: {},
  balanceInfo: {},
  userInfo: {},
  history: {},
  gameLogs: {},
  menu: [],
  categories: {},
  promotions: {
    categories: [],
    promotions: [],
    totalPage: 0
  },
  games: [],
  gameInfo: {},
  gamesCasinoOnline: [],
  wallet: {
    walletList: {},
    deposit: {},
    transfer: {}
  },
  changePassword: {},
  redirect: {},
  message: {},
  alert: [],
  clipboard: [],
  infoClient,
  country: {},
  status: {
    vi: {
      0: 'Đăng Chờ',
      1: 'Đang Xử Lý',
      2: 'Hoàn Tất',
      3: '',
      4: 'Tranh chấp',
      5: 'Chưa trả về',
      6: 'Trả về',
      7: 'Thua',
      8: '-',
      9: 'Thắng'
    },
    en: {
      0: 'Pending',
      1: 'InProccess',
      2: 'Completed',
      3: '',
      4: 'Confuse',
      5: 'Un-finished',
      6: 'Finished',
      7: 'Lose',
      8: '-',
      9: 'Win'
    },
    HISTORY: -1,
    LOGS: -5,
    BET_STATUS: -8
  }
}

export default function z88(state = initialState, action) {
  switch (action.type) {
    case types.DO_DEPOSIT_TRANSACTION_SUCCESS:
      return {
        ...state,
        wallet: {
          ...state.wallet,
          deposit: {
            orderId: action.payload.orderId
          }
        },
        redirect: {
          crossDomain: true,
          pathname: action.payload.paymentURL
        }
      }

    case types.DO_WITHDRAW_TRANSACTION:
      return {
        ...state,
        wallet: {
          ...state.wallet,
          withdraw: {
            eknutAccount: action.payload.eknutAccount
          }
        }
      }
    case types.DO_WITHDRAW_TRANSACTION_SUCCESS:
      return {
        ...state,
        redirect: {
          pathname: '/transaction-detail',
          state: {
            ...action.payload,
            type: 'withdraw',
            receiveAccount: state.wallet.withdraw.eknutAccount
          }
        }
      }

    case types.GET_CURRENT_BALANCE_STATUS:
      return {
        ...state,
        balanceInfo: {}
      }
    case types.GET_CURRENT_BALANCE_STATUS_SUCCESS:
      return {
        ...state,
        balanceInfo: {
          ...state.balanceInfo,
          ...action.payload
        }
      }
    case types.GET_CURRENT_BALANCE_STATUS_FAIL:
      return {
        ...state,
        balanceInfo: {}
      }

    case types.DO_TRANSFER_TRANSACTION: {
      const [firstTransaction, secondTransaction] = action.payload
      let fromAccount = MAIN_WALLET
      let toAccount = MAIN_WALLET
      const amount = firstTransaction.amount
      if (secondTransaction) {
        fromAccount = firstTransaction.productWallet
        toAccount = secondTransaction.productWallet
      } else if (firstTransaction.transferType !== 'deposit') {
        fromAccount = firstTransaction.productWallet
      } else {
        toAccount = firstTransaction.productWallet
      }
      return {
        ...state,
        wallet: {
          ...state.wallet,
          transfer: {
            fromAccount,
            toAccount,
            amount
          }
        }
      }
    }
    case types.DO_TRANSFER_TRANSACTION_SUCCESS:
      return {
        ...state,
        redirect: {
          pathname: '/transaction-detail',
          state: {
            ...action.payload,
            type: 'transfer',
            fromAccount: state.wallet.transfer.fromAccount,
            toAccount: state.wallet.transfer.toAccount,
            amount: state.wallet.transfer.amount
          }
        }
      }


    case types.GET_LOG_TO_LAUNCH_GAME_SUCCESS: {
      const idx = state.games.findIndex(x => x && x.gameId === action.payload.gameId)
      if (idx !== -1) {
        delete state.games[idx]
      }
      // if (action.payload.gameId === 1 || action.payload.gameId === 2) {
      //   return {
      //     ...state,
      //     games: [...state.games.filter(x => !!x), action.payload]
      //   }
      // }      

      if (action.payload.gameId === 1) {
        if (action.payload.gameURL !== undefined) {
          return {
            ...state,
            sportbookUrl: action.payload.gameURL
          }
        }
      } else if (action.payload.gameId === 2) {
        if (action.payload.gameURL !== undefined) {
          return {
            ...state,
            fishingGameUrl: action.payload.gameURL
          }
        }
      } else {
        // w1.location.href='http://www.google.com';
        action.payload.gameURL !== undefined
          // ? window.open(action.payload.gameURL, '', '')
          // : null
          ? action.payload.gameWindow.location.href = action.payload.gameURL
          : null
        action.payload.gameWindow.focus()
      }
     
      return state
    }
    case types.GET_LOG_TO_LAUNCH_GAME_FAIL: {
      const idx = state.games.findIndex(x => x && x.gameId === action.payload.gameId)
      if (idx !== -1) {
        delete state.games[idx]
      }
      if (action.payload.gameId === 1 || action.payload.gameId === 2) {
        return {
          ...state,
          games: []
        }
      }

      action.payload.gameURL !== undefined
        // ? window.open(action.payload.gameURL, '', '')
        // : null
        ? action.payload.gameWindow.location.href = action.payload.gameURL
        : null
      action.payload.gameWindow.focus()
      return state
    }
    case types.LAUNCH_TRY_IT_GAME_SUCCESS: {
      const idx = state.games.findIndex(x => x && x.gameId === action.payload.gameId)
      if (idx !== -1) {
        delete state.games[idx]
      }
      if (action.payload.gameId === 1 || action.payload.gameId === 2) {
        return {
          ...state,
          games: [...state.games.filter(x => !!x), action.payload]
        }
      }

      action.payload.gameURL !== undefined
        // ? window.open(action.payload.gameURL, '', '')
        // : null
        ? action.payload.gameWindow.location.href = action.payload.gameURL
        : null
      setTimeout(() => {
        action.payload.gameWindow.focus()
      }, 1000)
      return state
    }
    case types.GET_PROFILE_OF_USER_SUCCESS:
      return {
        ...state,
        userInfo: {
          ...state.userInfo,
          ...action.payload
        }
      }


    case types.GET_PROFILE_OF_USER_FAIL:
      return {
        ...state,
        authenticate: {},
        userInfo: {}
      }
    case types.CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        changePassword: {
          isSuccess: true
        }
      }
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        authenticate: {
          ...action.payload.authenticate,
          timeOut: new Date().getTime() + (60 * 60 * 1000)
        },
        userInfo: {
          ...state.userInfo,
          ...action.payload.userInfo
        },
        balanceInfo: {
          ...state.balanceInfo,
          ...action.payload.balanceInfo
        }
      }
    case types.LOGIN_FAIL:
      return {
        ...state,
        authenticate: {
          captcha: action.payload.captcha
        },
        balanceInfo: {},
        userInfo: {}
      }

    case types.LOGOUT_SUCCESS:
      return {
        ...state,
        userInfo: {},
        balanceInfo: {},
        authenticate: {},
        captcha: {},
        games: [],
        categories: {},
        message: {},
        alert: []
      }
    case types.LOGOUT_FAIL:
      return {
        ...state,
        userInfo: {},
        balanceInfo: {},
        authenticate: {},
        captcha: {},
        games: [],
        categories: {},
        message: {},
        alert: []
      }

    case types.REGISTER_ACCOUNT_BY_EMAIL_SUCCESS:
      return {
        ...state,
        authenticate: {
          ...state.authenticate,
          accessToken: action.payload.accessToken,
          timeOut: new Date().getTime() + (60 * 60 * 1000)
        }
      }
    case types.FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        redirect:
          {
            pathname: '/notification',
            state: {
              title: 'Xin kiểm tra email',
              content: ['Email lấy lại mật khẩu đã được gửi đến hồm thư của bạn', 'Vui lòng kiểm tra hộp thư và làm theo hướng dẫn'],
              icon: 'email',
              nextUrl: { pathname: '/resetpassword', state: { email: action.payload.email } }
            }
          }

      }

    case types.RESET_PASSWORD_FORGOT_SUCCESS:
      return {
        ...state,
        redirect: {
          pathname: '/notification',
          state: {
            title: 'Xác nhận',
            content: ['Phục hồi mật khẩu thành công'],
            icon: 'success',
            nextUrl: '/'
          }
        }
      }

    case types.GET_MENU_GAME_SUCCESS: {
      return {
        ...state,
        menu: action.payload.items
      }
    }

    case types.CHECK_ACCESS_TOKEN_FAIL:
      return {
        ...state,
        userInfo: {},
        balanceInfo: {},
        authenticate: {},
        captcha: {},
        games: [],
        categories: {},
        message: {},
        alert: []
      }

    case types.GET_PRODUCT_WALLET_SUCCESS: {
      const { walletList } = action.payload
      const refine = walletList.reduce((a, b) => {
        a[parseInt(b.walletId, 10)] = b.walletName
        return a
      }, { 0: 'Ví chính' })
      return {
        ...state,
        wallet: {
          ...state.wallet,
          walletList: refine
        }
      }
    }
    case types.GET_CAPTCHA_SUCCESS:
      return {
        ...state,
        captcha: action.payload
      }
    case types.GET_CAPTCHA_FAIL:
      return {
        ...state,
        captcha: {}
      }
    case types.START_TRANSACTION:
      return {
        ...state,
        wallet: {
          ...state.wallet,
          transfer: {},
          deposit: {},
          withdraw: {}
        }
      }
    case types.END_TRANSACTION:
      return {
        ...state,
        wallet: {
          ...state.wallet,
          transfer: {},
          deposit: {},
          withdraw: {}
        }
      }
    case types.MISSING_PROFILE:
      return {
        ...state,
        authenticate: action.payload.authenticate,
        userInfo: action.payload.userInfo,
        redirect: {
          pathname: '/updateprofile'
        }
      }
    case types.CLEAR_REDIRECT:
      return {
        ...state,
        redirect: {}
      }
    case types.HANDLE_ERROR: {
      if (action.payload && action.payload === responseCode.NOT_AUTHORIZED) {
        return {
          ...state,
          authenticate: {},
          userInfo: {}
        }
      }
      return {
        ...state
      }
    }
    case types.EMIT_MESSAGE: {
      const { type, message, authCode } = action.payload
      if (type) {
        state.message[type] = message
        return {
          ...state,
          message: {
            ...state.message
          }
        }
      } else if (authCode) {
        return {
          ...state,
          message: {
            ...state.message,
            content: message
          },
          authCode
        }
      }
      return {
        ...state,
        message: {
          ...state.message,
          content: message
        }
      }
    }
    case types.START_HANDLE_ERROR: {
      if (action.payload) {
        state.message[action.payload] = ''
      } else {
        state.message.content = ''
      }
      return {
        ...state,
        message: {
          ...state.message
        }
      }
    }

    case types.STOP_HANDLE_ERROR: {
      if (action.payload) {
        delete state[action.payload]
      } else {
        state.message.content = ''
      }
      return {
        ...state,
        message: {
          ...state.message
        }
      }
    }
    case types.COPY_TO_CLIPBOARD: {
      return {
        ...state,
        clipboard: Object.assign({}, { data: action.payload })
      }
    }
    case types.CLEAR_CLIPBOARD: {
      return {
        ...state,
        clipboard: Object.assign({})
      }
    }
    case types.CHANGE_CHANNEL: {
      return {
        ...state,
        infoClient: {
          ...state.infoClient
          // channel: action.payload ? action.payload : initialState.infoClient.channel
        }
      }
    }
    case types.CLEAR_CAPTCHA: {
      return {
        ...state,
        captcha: {},
        authenticate: { captcha: '' }
      }
    }
    case types.GET_HISTORY: {
      return {
        ...state,
        history: {}
      }
    }
    case types.GET_HISTORY_SUCCESS: {
      return {
        ...state,
        history: action.payload
      }
    }
    case types.GET_HISTORY_FAIL: {
      return {
        ...state,
        history: {}
      }
    }
    case types.GET_GAMES_LOGS: {
      return {
        ...state,
        gameLogs: {
          productWalletId: action.payload.productWalletId
        }
      }
    }
    case types.GET_GAMES_LOGS_SUCCESS: {
      return {
        ...state,
        gameLogs: {
          ...state.gameLogs,
          ...action.payload
        }
      }
    }
    case types.GET_GAMES_LOGS_FAIL: {
      return {
        ...state,
        gameLogs: {}
      }
    }
    case types.CLEAR_LOGS: {
      return {
        ...state,
        gameLogs: {}
      }
    }
    case types.GET_CATEGORIES_GAME_SUCCESS: {
      return {
        ...state,
        categories: Object.assign({}, state.categories, action.payload)
      }
    }
    case types.GET_GAME_BY_CATEGORY_SUCCESS: {
      const categories = {}
      for (let i = 0; i < action.payload.length; i++) {
        const item = action.payload[i]
        if (item.categoryId !== -1) {
          state.categories[item.productWallet].filter(x => x.categoryId === item.categoryId)[0].children.filter(x => x.categoryId === item.category)[0].children = item.data
        } else {
          state.categories[item.productWallet].filter(x => x.categoryId === item.category)[0].children = item.data
        }
      }

      const statesss = Object.assign({}, state.categories, categories)
      return {
        ...state,
        categories: Object.assign({}, state.categories, categories)
      }
    }
    case types.GET_GAME_CASINO_ONLINE_SUCCESS: {
      return {
        ...state,
        gamesCasinoOnline: action.payload
      }
    }
    case types.GET_GAME_CASINO_ONLINE_FAIL: {
      return {
        ...state,
        gamesCasinoOnline: []
      }
    }
    case types.CHECK_MISSING_PROFILE: {
      return {
        ...initialState
      }
    }
    case types.REMOVE_LOG_LAUNCH: {
      const idx = state.games.findIndex(x => x && x.gameId === action.payload.gameId)
      if (idx !== -1) {
        delete state.games[idx]
      }
      return {
        ...state,
        games: [...state.games.filter(x => !!x)]
      }
    }

    case types.GET_INVITE_TOP_20_SUCCESS:
      return {
        ...state,
        inviteTop20: {
          ...state.inviteTop20,
          ...action.payload
        }
      }

    case types.GET_INVITE_TOP_20_FAIL:
      return {
        ...state,
        inviteTop20: {}
      }

    case types.EMIT_ALERT: {
      return {
        ...state,
        alert: [...state.alert, action.payload]
      }
    }
    case types.GET_COUNTRY_SUCCESS: {
      return {
        ...state,
        country: action.payload
      }
    }
    case types.GET_CATEGORY_PROMOTION_SUCCESS: {
      const promotions = { ...state.promotions }
      promotions.categories = action.payload
      return {
        ...state,
        promotions
      }
    }
    case types.GET_BY_CATEGORY_PROMOTION_SUCCESS: {
      const promotions = { ...state.promotions }
      promotions.promotions = action.payload.promotionList
      promotions.totalPage = action.payload.totalPage
      return {
        ...state,
        promotions
      }
    }
    case types.GET_BY_CATEGORY_PROMOTION_FAIL: {
      const promotions = { ...state.promotions }
      promotions.promotions = []
      promotions.totalPage = 0
      return {
        ...state,
        promotions
      }
    }
    case types.GAME_GET_BY_ID_SUCCESS: {
      return {
        ...state,
        gameInfo: action.payload
      }
    }
    case types.GAME_GET_BY_ID_FAIL: {
      return {
        ...state,
        gameInfo: {}
      }
    }
    case types.SEND_OTP_VERIFY_EMAIL_SUCCESS: {
      return {
        ...state,
        dataSendOptVerify: action.payload,
        message: {}
      }
    }
    case types.SEND_OTP_VERIFY_EMAIL_FAIL: {
      return {
        ...state,
        dataSendOptVerify: {}
      }
    }
    case types.SEND_AUTHEN_OTP_SUCCESS: {
      return {
        ...state,
        authenticate: {
          ...state.authenticate,
          accessToken: action.payload.accessToken,
          timeOut: new Date().getTime() + (60 * 60 * 1000)
        }
      }
    }
    case types.SEND_AUTHEN_OTP_FAIL: {
      return {
        ...state,
        message: action.payload.message
      }
    }
    case types.GET_PAGE_OF_ALIAS_SUCCESS: {
      return {
        ...state,
        article: action.payload
      }
    }
    case types.GET_PAGE_OF_ALIAS_FAIL: {
      return {
        ...state,
        article: {}
      }
    }
    case types.GET_BTC_PAYMENT_INFO_SUCCESS: {
      return {
        ...state,
        BTCPaymentInfo: action.payload
      }
    }
    case types.GET_BTC_PAYMENT_INFO_FAIL: {
      return {
        ...state,
        BTCPaymentInfo: {}
      }
    }
    default:
      return state
  }
}
