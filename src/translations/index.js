import { vi, en } from '../locales'

const translations = {
  vi,
  en
}
export default translations
