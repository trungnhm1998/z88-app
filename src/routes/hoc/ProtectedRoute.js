import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
// import { browserHistory } from 'react-router'
import _ from 'lodash'
import * as z88Actions from '../../redux/z88/z88.actions'

export default function ProtectedRoute(Component) {
  class AuthenticationComponent extends React.Component {
    componentWillMount() {
      const { z88Action, accessToken } = this.props
      if (_.isEmpty(accessToken)) {
        // browserHistory.push('/')
      } else {
        z88Action.checkAccessToken({})
      }
    }

    componentWillUpdate(nextProps) {
      const { accessToken } = nextProps
      if (_.isEmpty(accessToken)) {
        // browserHistory.push('/')
      }
    }

    render() {
      return (
        <Component {...this.props} />
      )
    }
  }

  return connect(
    (rootState) => {
      return {
        accessToken: rootState.z88.authenticate.accessToken
      }
    },
    (dispatch) => { return { z88Action: bindActionCreators(z88Actions, dispatch) } }
  )(AuthenticationComponent)
}
