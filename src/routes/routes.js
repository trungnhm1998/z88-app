import React, { Component } from 'react'
import {
  Scene,
  Router,
  Stack,
  Drawer,
  Actions
} from 'react-native-router-flux'

import * as Components from '../components'


class RouterComponent extends Component {
  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene hideNavBar key="game" initial component={Components.Home} />
          <Scene hideNavBar key="wallet" component={Components.Wallet} />
          <Scene hideNavBar key="betting" component={Components.Betting} />
          <Scene hideNavBar key="account" component={Components.Account} />
          <Stack hideNavBar key="signin">
            <Scene hideNavBar direction="vertical" initial component={Components.Signin} />
            <Scene hideNavBar key="signup" component={Components.Signup} />
          </Stack>
        </Scene>
      </Router>
    )
  }
}

export default RouterComponent
