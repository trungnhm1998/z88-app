import axios from 'axios'
import base64url from 'base64-url'
import { getAccessToken, getLanguage, getInfoClient } from '../../selector'
import store from '../../configs/store.config'


const api = ({
  method,
  url,
  headers = {},
  data
}) => {
  const language = getLanguage(store.getState())
  headers.lang = language
  headers['accept-language'] = language
  return new Promise(async (resolve, reject) => {
    try {
      const response = await axios({
        method,
        url,
        data,
        headers
      })

      resolve(response.data)
    } catch (error) {
      if (error) console.log(error)
      reject(error)
    }
  })
}


const callAPI = ({
  method,
  url,
  headers,
  data
}) => api({
  method, url, headers, data 
})

const callAPIWithClientInfo = ({
  method, url, headers = {}, data 
}) => {
  const infoClient = getInfoClient(store.getState())
  return api({
    method, url, headers: { ...headers, infoClient: base64url.encode(JSON.stringify(infoClient)) }, data 
  })
}

const callAPIWithAccessToken = ({
  method, url, headers, data 
}) => {
  const accessToken = getAccessToken(store.getState())
  return api({
    method, url, headers: { ...headers, accessToken }, data 
  })
}

const Request = {
  callAPI, callAPIWithClientInfo, callAPIWithAccessToken
}

export default Request
