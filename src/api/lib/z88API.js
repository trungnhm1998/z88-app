import { API_Z88_URL } from '../../configs/api.config'
import Request from '../middleware/request'

const z88API = {
  updateProfileOfUser(arg, accessToken) {
    if (accessToken) {
      return Request.callAPI({
        method: 'post', url: `${API_Z88_URL}/user/updateProfile`, headers: { accessToken }, data: { ...arg }
      })
    }
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/user/updateProfile`, data: { ...arg } })
  },
  updateAvatarOfUser(arg) {
    return Request.callAPI({ method: 'post', url: `${API_Z88_URL}/user/updateAvatar`, data: { ...arg } })
  },
  resetPasswordForgot(arg) {
    return Request.callAPIWithClientInfo({ method: 'post', url: `${API_Z88_URL}/user/setForgotPassword`, data: { ...arg } })
  },
  sendEmailToResetPasswordForgot(arg) {
    return Request.callAPIWithClientInfo({ method: 'post', url: `${API_Z88_URL}/user/forgotPassword`, data: { ...arg } })
  },
  sendAuthenOtp(arg) {
    return Request.callAPIWithClientInfo({ method: 'post', url: `${API_Z88_URL}/user/LoginStep2`, data: { ...arg } })
  },
  registerAccountByEmail(arg) {
    return Request.callAPIWithClientInfo({ method: 'post', url: `${API_Z88_URL}/user/register`, data: { ...arg } })
  },
  logout(arg) {
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/user/logout`, data: { ...arg } })
  },
  login(arg) {
    return Request.callAPIWithClientInfo({ method: 'post', url: `${API_Z88_URL}/user/login`, data: { ...arg } })
  },
  changePassword(arg) {
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/user/changePassword`, data: { ...arg } })
  },
  getProfileOfUser(arg, accessToken) {
    if (accessToken) {
      return Request.callAPI({
        method: 'post', url: `${API_Z88_URL}/user/getProfile`, headers: { accessToken }, data: { ...arg }
      })
    }
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/user/getProfile`, data: { ...arg } })
  },
  getLogToLaunchGame(arg) {
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/game/mobileLaunch`, data: { ...arg } })
  },
  LaunchTryItGame(arg) {
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/game/launchTryIt`, data: { ...arg } })
  },
  getInformationOfJackpot(arg) {
    return Request.callAPI({ method: 'post', url: `${API_Z88_URL}/game/getJackpot`, data: { ...arg } })
  },
  getCategoriesGame(arg) {
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/game/getCategories`, data: { ...arg } })
  },
  getGameByCategory(arg) {
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/game/getByCategory`, data: { ...arg } })
  },
  gameGetById(arg) {
    return Request.callAPI({ method: 'post', url: `${API_Z88_URL}/game/getById`, data: { ...arg } })
  },
  getGameCasinoOnline() {
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/game/getGameCasinoOnline` })
  },
  getTransferTransactionStatus(arg) {
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/balance/transferStatus`, data: { ...arg } })
  },
  doTransferTransaction(arg) {
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/balance/transfer`, data: { ...arg } })
  },
  doWithdrawTransaction(arg) {
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/balance/withdraw`, data: { ...arg } })
  },
  getPaymentMethods(arg) {
    return Request.callAPI({ method: 'post', url: `${API_Z88_URL}/balance/getPaymentMethod`, data: { ...arg } })
  },
  getHistory(arg) {
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/balance/getHistory`, data: { ...arg } })
  },
  getTransferHistory(arg) {
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/balance/getTransferHistory`, data: { ...arg } })
  },
  getGameLogs(arg) {
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/game/getGameLogs`, data: { ...arg } })
  },
  getCurrentBalanceStatus(arg, accessToken) {
    if (accessToken) {
      return Request.callAPI({
        method: 'post', url: `${API_Z88_URL}/balance/getCurrent`, headers: { accessToken }, data: { ...arg }
      })
    }
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/balance/getCurrent`, data: { ...arg } })
  },
  getDepositTransactionResult(arg) {
    return Request.callAPI({ method: 'post', url: `${API_Z88_URL}/balance/depositResult`, data: { ...arg } })
  },
  doDepositTransaction(arg) {
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/balance/deposit`, data: { ...arg } })
  },
  getMenuGame() {
    return Request.callAPIWithAccessToken({ method: 'get', url: `${API_Z88_URL}/setting/getMenu` })
  },
  getProductWallet() {
    return Request.callAPI({ method: 'get', url: `${API_Z88_URL}/setting/getProductWallet` })
  },
  getCountry() {
    return Request.callAPI({ method: 'get', url: `${API_Z88_URL}/setting/getCountry` })
  },
  getCaptcha(arg) {
    return Request.callAPIWithClientInfo({ method: 'post', url: `${API_Z88_URL}/user/getCaptcha`, data: { ...arg } })
  },
  getInviteTop20() {
    return Request.callAPI({ method: 'get', url: `${API_Z88_URL}/setting/getInviteTop20` })
  },
  getCategoryPromotion() {
    return Request.callAPI({ method: 'post', url: `${API_Z88_URL}/promotion/getCategory` })
  },
  getByCategoryPromotion(arg) {
    return Request.callAPI({ method: 'post', url: `${API_Z88_URL}/promotion/getByCategory`, data: { ...arg } })
  },
  sendOtpVerifyEmail(arg) {
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/user/sendVerifyEmail`, data: { ...arg } })
  },
  getPageOfAlias(arg) {
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/article/getPageOfAlias`, data: { ...arg } })
  },
  getBTCPaymentInfo() {
    return Request.callAPIWithAccessToken({ method: 'post', url: `${API_Z88_URL}/paymentBtc/getPaymentInfo` })
  }
}
export default z88API
