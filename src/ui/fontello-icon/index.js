import { createIconSetFromFontello } from 'react-native-vector-icons'
import fontelloConfig from '../../configs/elloIcon.config.json'

const Icon = createIconSetFromFontello(fontelloConfig)
export default Icon
