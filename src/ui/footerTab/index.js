import React from 'react'
import { View } from 'react-native'
import { Text, Icon } from 'native-base'
import { Metrics } from '../themes'

const TabIcon = ({
  focused, title, iconType, iconName 
}) => {
  return (
    <View
      style={{
        backgroundColor: focused ? 'rgb(164, 15, 16)' : 'rgb(28, 28, 28)',
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
      }}
    >
      <Icon
        style={{ color: 'white', fontSize: 22 * Metrics.ratioScreen }}
        type={iconType}
        name={iconName}
      />
      <Text style={{ color: 'white', fontSize: 9 * Metrics.ratioScreen }}>{title}</Text>
    </View>
  )
}

export default TabIcon
