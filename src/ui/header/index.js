import React from 'react'
import {
  View,
  Image,
  StyleSheet,
  Platform
} from 'react-native'
import { Metrics, Images } from '../themes'

const styles = StyleSheet.create({
  HeaderContainer: {
    height: Metrics.navBarHeight,
    borderBottomColor: 'rgb(144, 34, 6)',
    borderBottomWidth: 2
  },
  BackgroundImage: {
    flex: 1,
    width: null,
    height: Metrics.navBarHeight,
    resizeMode: 'cover'
  },
  HeaderContentContainer: {
    position: 'absolute',
    width: '100%',
    height: '100%'
  }
})

// Header with background images

const Header = ({ children }) => {
  return (
    <View style={[{ backgroundColor: 'rgba(0, 0, 0, 0.3)', ...styles.HeaderContainer }]}>
      <Image style={styles.BackgroundImage} source={Images.headerBackground} />
      <View style={styles.HeaderContentContainer}>
        {children}
      </View>
    </View>
  )
}

export default Header
