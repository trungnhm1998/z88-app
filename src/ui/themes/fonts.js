import Metrics from './metrics'

const size = {
  h1: 38 * Metrics.ratioScreen,
  h2: 34 * Metrics.ratioScreen,
  h3: 30 * Metrics.ratioScreen,
  h4: 26 * Metrics.ratioScreen,
  h5: 20 * Metrics.ratioScreen,
  h6: 19 * Metrics.ratioScreen,
  input: 18 * Metrics.ratioScreen,
  regular: 17 * Metrics.ratioScreen,
  medium: 14 * Metrics.ratioScreen,
  small: 12 * Metrics.ratioScreen,
  tiny: 8.5 * Metrics.ratioScreen
}

export default {
  size
}
