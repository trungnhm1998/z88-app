import Fonts from './fonts'
import Metrics from './metrics'
import Colors from './colors'


const styles = {
  HeaderRightButton: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  HeaderLeftButton: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  HeaderContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
}

export default styles
