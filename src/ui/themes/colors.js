const colors = {
  blackBackground: '#000',
  mainBackground: 'rgb(7, 7, 7)',
  headerText: '#fff'
}

export default colors
