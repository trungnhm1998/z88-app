export default {
  headerBackground: require('./images/header-background.png'),
  headerLogo: require('./images/logo.png'),
  homeBanner: require('./images/top-banner.jpg'),
  homeBannerEn: require('./images/top-banner-en.jpg'),
  homePromotionEn: require('./images/promotion-en.jpg'),
  homePromotionVi: require('./images/promotion-vi.jpg'),
  newGameEn: require('./images/new-games-en.jpg'),
  newGameVi: require('./images/new-games.jpg'),
  sportBetEn: require('./images/sportbet-en.jpg'),
  sportBetVi: require('./images/sportbet-vi.jpg'),
  backgroundImage: require('./images/background-image.png')
}
