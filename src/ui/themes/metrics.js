import { Dimensions, Platform } from 'react-native'

const { width, height } = Dimensions.get('window')

const ratioScreen = width / 375

const metrics = {
  marginHorizontal: 20,
  marginVertical: 20,
  paddingHorizontal: 15,
  paddingVertical: 15,
  section: 25,
  baseMargin: 10,
  doubleBaseMargin: 20,
  smallMargin: 5,
  horizontalLineHeight: 1,
  searchBarHeight: 30,
  screenWidth: width < height ? width : height,
  screenHeight: width < height ? height : width,
  navBarHeight: (Platform.OS === 'ios') ? 64 : 54,
  buttonRadius: 7,

  inputHeight: 30,
  smallButtonHeight: 25,
  smallButtonRadiusHeight: '3%',
  buttonHeight: '7%',
  buttonWidth: '60%',
  buttonBorderRadiusByHeight: '4%',
  borderRadius: 4,
  widthContainer: '95%',
  fullWidthContainer: '100%',
  paddingLeft: 20,
  paddingRight: 20,
  iconMargin: 14,
  toolBar: 56,

  ratioScreen
}

export default metrics
