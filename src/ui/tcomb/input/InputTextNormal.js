const React = require('react')
const {
  View,
  Text,
  TextInput,
  Image,
  StyleSheet,
  PixelRatio
} = require('react-native')

const styles = StyleSheet.create({
  inputStyle: {
    flex: 1
  }
})

function InputTextNormal(locals) {
  if (locals.hidden) {
    return null
  }

  const stylesheet = locals.stylesheet
  let formGroupStyle = stylesheet.formGroup.normal
  let controlLabelStyle = stylesheet.controlLabel.normal
  let textboxStyle = stylesheet.textbox.normal
  let textboxViewStyle = stylesheet.textboxView.normal
  let helpBlockStyle = stylesheet.helpBlock.normal
  let errorBlockStyle = stylesheet.errorBlock

  if (locals.hasError) {
    formGroupStyle = stylesheet.formGroup.error
    controlLabelStyle = stylesheet.controlLabel.error
    textboxStyle = stylesheet.textbox.error
    textboxViewStyle = stylesheet.textboxView.error
    helpBlockStyle = stylesheet.helpBlock.error
  }

  if (locals.editable === false) {
    formGroupStyle = stylesheet.formGroup.normal
    controlLabelStyle = stylesheet.controlLabel.normal
    textboxStyle = stylesheet.textbox.normal
    textboxViewStyle = stylesheet.textboxView.normal
    helpBlockStyle = stylesheet.helpBlock.normal
    errorBlockStyle = stylesheet.errorBlock
  }

  const label = locals.config.label ? <Text style={[{ color: '#000' }, controlLabelStyle, locals.config.labelStyle]}>{locals.config.label}</Text> : null
  const help = locals.help ? <Text style={helpBlockStyle}>{locals.help}</Text> : null
  const error = locals.hasError && locals.error ? <Text accessibilityLiveRegion="polite" style={errorBlockStyle}>{locals.error}</Text> : null

  return (
    <View style={formGroupStyle}>
      {label}
      <View style={[{ flexDirection: 'row', paddingVertical: 5, backgroundColor: '#fff' }, textboxViewStyle]}>
        <TextInput
          accessibilityLabel={locals.label}
          ref="input"
          autoCapitalize={locals.autoCapitalize}
          autoCorrect={false}
          autoFocus={locals.autoFocus}
          blurOnSubmit={locals.blurOnSubmit}
          editable={locals.editable}
          keyboardType={locals.keyboardType}
          maxLength={locals.maxLength}
          multiline={locals.multiline}
          onBlur={locals.onBlur}
          onEndEditing={locals.onEndEditing}
          onFocus={locals.onFocus}
          onLayout={locals.onLayout}
          onSelectionChange={locals.onSelectionChange}
          onSubmitEditing={locals.onSubmitEditing}
          onContentSizeChange={locals.onContentSizeChange}
          placeholderTextColor={locals.placeholderTextColor}
          secureTextEntry={locals.secureTextEntry}
          selectTextOnFocus={locals.selectTextOnFocus}
          selectionColor={locals.selectionColor}
          numberOfLines={locals.numberOfLines}
          underlineColorAndroid={locals.underlineColorAndroid}
          clearButtonMode={locals.clearButtonMode}
          clearTextOnFocus={locals.clearTextOnFocus}
          enablesReturnKeyAutomatically={locals.enablesReturnKeyAutomatically}
          keyboardAppearance={locals.keyboardAppearance}
          onKeyPress={locals.onKeyPress}
          returnKeyType={locals.returnKeyType}
          selectionState={locals.selectionState}
          onChangeText={value => locals.onChange(value)}
          onChange={locals.onChangeNative}
          placeholder={locals.placeholder}
          style={[textboxStyle, styles.inputStyle]}
          value={locals.value}
        />
      </View>
      {help}
      {error}
    </View>
  )
}

module.exports = InputTextNormal
