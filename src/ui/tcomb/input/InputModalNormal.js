import React from 'react'
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native'
import { Icon } from '../../../ui'
import _ from 'lodash'
import t from 'tcomb-form-native'

const Textbox = t.form.Textbox
class InputModalNormal extends Textbox {
  constructor(props) {
    super(props)
    this.state = {
      // fieldFocused: (props.value) ? true : false,
      // value: (props.value) ? String(props.value) : undefined,
      // fadeAnim: (props.value) ? new Animated.Value(1) : new Animated.Value(0),
      // placeholderString: undefined,
      // isModalVisible: false
    }
  }

  getTemplate = () => {
    let self = this
    return (locals) => {
      if (locals.hidden) {
        return null
      }
      const stylesheet = locals.stylesheet
      let formGroupStyle = stylesheet.formGroup.normal
      let controlLabelStyle = stylesheet.controlLabel.normal
      let textboxStyle = stylesheet.modal.normal
      let helpBlockStyle = stylesheet.helpBlock.normal
      let errorBlockStyle = stylesheet.errorBlock
      if (locals.hasError) {
        controlLabelStyle = stylesheet.controlLabel.error
        formGroupStyle = stylesheet.formGroup.error
        textboxStyle = stylesheet.modal.error
        helpBlockStyle = stylesheet.helpBlock.error
      }
      if (locals.editable === false) {
        textboxStyle = stylesheet.textbox.notEditable
      }
      const help = locals.help ? <Text style={helpBlockStyle}>{locals.help}</Text> : null
      const error = locals.hasError && locals.error ? <Text style={errorBlockStyle}>{locals.error}</Text> : null
      // var label = locals.config.label ? <Text style={controlLabelStyle}>{locals.config.label}</Text> : null
      var label = locals.config.label ? <Text style={[controlLabelStyle,{color:'#000'},locals.config.labelStyle]}>{locals.config.label}</Text> : null;
      const valueModal = _.isNil(locals.value) ? <Text style={{color:locals.placeholderTextColor}}>{locals.placeholder}</Text> : <Text>{locals.value}</Text>

      return (
        <View style={formGroupStyle}>
          {label}
          <TouchableOpacity ref="input" activeOpacity={0.8} style={[textboxStyle, { height: 47, justifyContent: 'center',}]} onPress={locals.onFocus}>
            <Text>{valueModal}</Text>
          </TouchableOpacity>
          {help}
          {error}
        </View>
      )
    }
  }

}

const styles = StyleSheet.create({
  textInput: {
    height: 40
  }
})

export default InputModalNormal
