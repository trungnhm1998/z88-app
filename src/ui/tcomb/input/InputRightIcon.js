import { TouchableOpacity } from 'react-native'
// import VectorIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import { Icon } from 'native-base'
import { FontelloIcon } from '../../'

const React = require('react')
const {
  View, Text, TextInput, StyleSheet 
} = require('react-native')

const styles = StyleSheet.create({
  inputStyle: {
    flex: 1
  }
})

function InputRightIcon(locals) {
  console.log('locals', locals)
  let input = null

  if (locals.hidden) {
    return null
  }

  const stylesheet = locals.stylesheet
  let formGroupStyle = stylesheet.formGroup.normal
  let controlLabelStyle = stylesheet.controlLabel.normal
  let textboxStyle = stylesheet.textbox.normal
  let textboxViewStyle = stylesheet.textboxView.normal
  let helpBlockStyle = stylesheet.helpBlock.normal
  const errorBlockStyle = stylesheet.errorBlock

  if (locals.hasError) {
    formGroupStyle = stylesheet.formGroup.error
    controlLabelStyle = stylesheet.controlLabel.error
    textboxStyle = stylesheet.textbox.error
    textboxViewStyle = stylesheet.textboxView.error
    helpBlockStyle = stylesheet.helpBlock.error
  }

  if (locals.editable === false) {
    textboxStyle = stylesheet.textbox.normal
    textboxViewStyle = stylesheet.textboxView.normal
  }

  const label = locals.config.label ? <Text style={[{ color: '#000' }, controlLabelStyle, locals.config.labelStyle]}>{locals.config.label}</Text> : null
  const help = locals.help ? <Text style={helpBlockStyle}>{locals.help}</Text> : null
  const error = locals.hasError && locals.error ? <Text accessibilityLiveRegion="polite" style={errorBlockStyle}>{locals.error}</Text> : null
  return (
    <View style={formGroupStyle}>
      {label}
      <View style={[{ flexDirection: 'row', paddingVertical: 5, backgroundColor: '#fff' }, textboxViewStyle, locals.config.inputStyle]}>
        <TextInput
          accessibilityLabel={locals.label}
          ref={(ref) => {
            console.log(ref)
            input = ref
          }}
          autoCapitalize={locals.autoCapitalize}
          autoCorrect={false}
          autoFocus={locals.autoFocus}
          blurOnSubmit={locals.blurOnSubmit}
          editable={locals.editable}
          keyboardType={locals.keyboardType}
          maxLength={locals.maxLength}
          multiline={locals.multiline}
          onBlur={locals.onBlur}
          onEndEditing={locals.onEndEditing}
          onFocus={locals.config.onFocus}
          onLayout={locals.onLayout}
          onSelectionChange={locals.onSelectionChange}
          onSubmitEditing={locals.onSubmitEditing}
          onContentSizeChange={locals.onContentSizeChange}
          placeholderTextColor={locals.placeholderTextColor}
          secureTextEntry={locals.secureTextEntry}
          selectTextOnFocus={locals.selectTextOnFocus}
          selectionColor={locals.selectionColor}
          numberOfLines={locals.numberOfLines}
          underlineColorAndroid={locals.underlineColorAndroid}
          clearButtonMode={locals.clearButtonMode}
          clearTextOnFocus={locals.clearTextOnFocus}
          enablesReturnKeyAutomatically={locals.enablesReturnKeyAutomatically}
          keyboardAppearance={locals.keyboardAppearance}
          onKeyPress={locals.onKeyPress}
          returnKeyType={locals.returnKeyType}
          selectionState={locals.selectionState}
          onChangeText={value => locals.onChange(value)}
          onChange={locals.onChangeNative}
          placeholder={locals.placeholder}
          style={[textboxStyle, styles.inputStyle, { borderRadius: 10 }]}
          value={locals.value}
        />
        <TouchableOpacity onPress={locals.config.onFocus} style={{ width: 30, justifyContent: 'center' }}>
          {
          locals.config.useVectorIcon !== true
            ? <FontelloIcon
              name={locals.config.rightIcon}
              size={locals.config.rightIconSize}
              color={locals.config.rightIconColor}
              style={{ alignSelf: 'flex-end', paddingRight: 35 }}
            />
            : <Icon
              type={locals.config.rightIconType}
              name={locals.config.rightIconName}
              color={locals.config.rightIconColor}
              size={locals.config.rightIconSize}
              style={{ alignSelf: 'flex-end', paddingRight: 35 }}
            />
          }
        </TouchableOpacity>
      </View>
      {help}
      {error}
    </View>
  )
}


InputRightIcon.defaultProps = {
  // rightIconColor: 'red'
}


module.exports = InputRightIcon
