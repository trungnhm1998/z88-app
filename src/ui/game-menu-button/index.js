import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  TouchableWithoutFeedback 
} from 'react-native'
import { Metrics } from '../themes'
import Icon from '../fontello-icon'

const styles = StyleSheet.create({
  buttonContainer: {
    width: 70 * Metrics.ratioScreen,
    height: 70 * Metrics.ratioScreen,
    marginHorizontal: 10,
    justifyContent: 'flex-end',
    flexDirection: 'column',
    alignItems: 'center',
    borderRadius: 8,
    borderColor: 'rgb(144, 122, 20)',
    borderWidth: 1
  },
  title: {
    color: 'white',
    marginBottom: 5,
    fontSize: 10 * Metrics.ratioScreen
  },
  iconContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  icon: {
    color: '#ffd411'
  }
})

const GameMenuButton = ({
  title, iconName, onPress 
}) => {
  console.log(iconName)
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.buttonContainer}>
        <View style={styles.iconContainer}>
          {
            <Icon style={styles.icon} name={iconName} size={20 * Metrics.ratioScreen} />
          }
        </View>
        <Text style={styles.title}>
          {title}
        </Text>
      </View>
    </TouchableWithoutFeedback>
  )
}

export default GameMenuButton
