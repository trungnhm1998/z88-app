import React, { Component } from 'react'
import AppRoute from '../routes/routes'

const Router = () => {
  return (
    <AppRoute />
  )
}

export default Router
