import { combineReducers } from 'redux'
import { i18nState } from 'redux-i18n'
import apiReducer from '../redux/api/api.reducers'
import z88 from '../redux/z88/z88.reducers'

const rootReducer = combineReducers({
  apiResponse: apiReducer,
  z88,
  i18nState
})

export default rootReducer
