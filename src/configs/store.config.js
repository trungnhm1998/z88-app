import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'

import rootReducer from './reducer.config'
import rootSaga from '../sagas'

const configureStore = (initialState) => {
  const sagaMiddleware = createSagaMiddleware()
  let store = {}
  store = createStore(
    rootReducer,
    initialState,
    compose(
      applyMiddleware(sagaMiddleware)
    )
  )
  sagaMiddleware.run(rootSaga)
  return store
}

const store = configureStore({})
export default store
