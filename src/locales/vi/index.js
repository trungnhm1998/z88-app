import _ from 'lodash'
import html from './vi.html.locale'
import vis from './vi.locale'

const vi = _.assign(html, vis)
export { vi }
