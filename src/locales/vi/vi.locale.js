export default {
  transfer: 'Chuyển tiền',
  withdraw: 'Rút tiền',
  deposit: 'Gửi tiền',
  signup: 'Đăng ký mới',
  login: 'Đăng nhập',
  male: 'Nam',
  female: 'Nữ',
  fullName: 'Họ và tên',
  birthDay: 'Ngày sinh',
  gender: 'Giới tính',
  email: 'Email',
  password: 'Mật khẩu',
  phone: 'Số điện thoại',
  identifyCard: 'CMND',
  nationality: 'Quốc tịch',
  address: 'Địa chỉ',
  currency: 'Đơn vị tiền tệ',
  language: 'Ngôn ngữ',
  favoriteGameWallet: 'Ví trò chơi yêu thích',
  agency: 'Mã đại lý',
  captcha: 'Captcha',

  // Button
  signupBtn: 'Đăng ký',
  cancelBtn: 'Hủy bỏ',
  tryAgainBtn: 'Thử lại',
  finishBtn: 'Hoàn tất',
  withdrawBtn: 'RÚT TIỀN RA',

  // Sign up page
  // placeholder
  firstNamePH: 'Họ',
  lastNamePH: 'Tên',
  emailPH: 'Địa chỉ Email',
  confirmEmailPH: 'Nhập lại địa chỉ Email',
  passwordPH: 'Mật khẩu',
  confirmPasswordPH: 'Xác nhận lại mật khẩu',
  phoneNumberPH: 'Nhập số điện thoại',
  idCardPH: 'Nhập số CMND',
  languagePH: 'Chọn ngôn ngữ',
  addressPH: 'Nhập địa chỉ',
  nationalityPH: 'Chọn quốc tịch của bạn',
  currencyPH: 'Chọn loại tiền tệ',
  favoriteWalletPH: 'Chọn ví',
  captchaPH: 'Captcha',
  agencyPH: 'Nhập mã đại lý',

  profile: 'Thông tin cơ bản',
  detailInformation: 'Thông tin chi tiết',

  createNewAccount: 'Đăng ký mới',

  agreeAndAccept: 'Tôi hiểu và chấp nhận các chính sách của Z88',
  confirmAge: 'Tôi xác nhận mình đủ 18 tuổi',
  depositSuccess: 'Chúc mừng bạn thanh toán thành công\nHãy vào sòng và bắt đầu canh bạc của đời mình',
  depositFailed: 'Có lỗi xảy ra trong quá trình giao dịch\nVui lòng kiểm tra và thử lại ngay',
  transferSuccess: 'Chúc mừng bạn thanh toán thành công\nHãy vào sòng và bắt đầu canh bạc của đời mình',
  withdrawSuccess: 'Chúc mừng bạn thanh toán thành công\nHãy vào sòng và bắt đầu canh bạc của đời mình',

  // status
  process: 'Đang xử lý',
  success: 'Thành công',
  failed: 'Thất bại',

  // invoice
  transactionTime: 'Thời gian xác nhận giao dịch',
  fromAccount: 'Từ tài khoản',
  toAccount: 'Đến tài khoản',
  invoiceId: 'Mã giao dịch',
  amount: 'Số tiền',
  refId: 'Mã tham chiếu',
  rateOfExchange: 'Tỷ giá lúc giao dịch',
  receiveAccount: 'Tài khoản nhận',
  'Giới Thiệu': 'Giới Thiệu',
  'Hướng dẫn chơi': 'Hướng dẫn chơi',
  'Hỗ trợ trực tuyến': 'Hỗ trợ trực tuyến',
  'HỖ TRỢ TRỰC TUYẾN ': 'HỖ TRỢ TRỰC TUYẾN ',
  'Khuyến mãi': 'Khuyến mãi',
  'Đại lý': 'Đại lý',
  'Đăng ký mới': 'Đăng ký mới',
  'You must login to play game': 'Bạn phải đăng nhập để chơi game',
  'Quên mật khẩu': 'Quên mật khẩu',
  'Đánh bạc có trách nhiệm': 'Đánh bạc có trách nhiệm',
  'Điều kiện và điều khoản': 'Điều kiện và điều khoản',
  'Tuyên bố trách nhiệm': 'Tuyên bố trách nhiệm',
  'Chính sách bảo mật': 'Chính sách bảo mật',
  'Quy định và luật lệ': 'Quy định và luật lệ',
  'Giấy phép chứng nhận': 'Giấy phép chứng nhận',
  'Phương thức thanh toán': 'Phương thức thanh toán',
  'Công nghệ bảo mật': 'Công nghệ bảo mật',
  'Trách nhiệm': 'Trách nhiệm',
  'Theo dõi Z88 trên:': 'Theo dõi Z88 trên:',
  'text foooter': 'Z88 thuộc về UNOGLOBAL LTD. Được cấp phép bởi Belize Gambling cho sòng bạc trực tuyến. Belize Computer Licensing Board chịu trách nhiệm kiểm soát và điều chỉnh các hoạt động chơi game trực tuyến.',

  // text static page
  'Thông tin chung': 'Thông tin chung',
  'Giới thiệu công ty': 'Giới thiệu công ty',
  'Hướng dẫn': 'Hướng dẫn',
  'Những câu hỏi thường gặp': 'Những câu hỏi thường gặp',
  'Qui Định Chung': 'Quy định chung',
  // tranlage friend
  'Thưởng nóng khi giới thiệu bạn bè thành công': 'Thưởng nóng khi giới thiệu bạn bè thành công',
  'Sử dụng đường link để gửi đến bạn bè và nhận giải thưởng ngay': 'Sử dụng đường link để gửi đến bạn bè và nhận giải thưởng ngay',
  'Sao chép': 'Sao chép',
  'Người được giới thiệu phải nạp tiền thành công và số tiền ít nhất 100.000 VNĐ': 'Người được giới thiệu phải nạp tiền thành công và số tiền ít nhất 100.000 VNĐ',
  'Gửi tiền ngay': 'Gửi tiền ngay',
  'Rút tiền': 'Rút Tiền',
  'Chuyển khoản': 'Chuyển khoản',
  'Lịch sử': 'Lịch sử',
  'Đổi mật khẩu': 'Đổi mật khẩu',
  'Thông tin tài khoản': 'Thông tin tài khoản',
  'Đăng xuất': 'Đăng Xuất',
  'Gửi tiền': 'Gửi Tiền',
  'Hướng dẫn chơi game thể thao': 'Hướng dẫn chơi game thể thao',
  'Tài Khoản Của Tôi': 'Tài Khoản Của Tôi',
  'Đặt Cọc': 'Đặt cọc',
  'Thể thao': 'Thể thao',
  'Trò chơi': 'Trò chơi',
  'Câu hỏi chung': 'Câu hỏi chung',
  'Tài khoản của tôi': 'Tài khoản của tôi',
  'Đặt cọc': 'Đặt cọc',
  'Mật khẩu mới không được giống mật khẩu cũ': 'Mật khẩu mới không được giống mật khẩu cũ',
  'Xác nhận mật khẩu mới không chính xác': 'Xác nhận mật khẩu mới không chính xác',
  'Mật khẩu cũ': 'Mật khẩu cũ',
  'Mật khẩu phải nhiều hơn 6 ký tự': 'Mật khẩu phải nhiều hơn 6 ký tự',
  'Mật khẩu mới': 'Mật khẩu mới',
  'Nhập lại mật khẩu mới': 'Nhập lại mật khẩu mới',
  'Vào tài khoản z88': 'Vào tài khoản z88',
  'Đổi mật khẩu thành công. Vui lòng đăng nhập lại': 'Đổi mật khẩu thành công. Vui lòng đăng nhập lại',
  'Tự động chuyển tới trang chủ sau': 'Tự động chuyển tới trang chủ sau',
  'Xin kiểm tra email': 'Xin kiểm tra email',
  'Email lấy lại mật khẩu đã được gửi đến': 'Email lấy lại mật khẩu đã được gửi đến',
  'Vui lòng kiểm tra hộp thư và làm theo hướng dẫn': 'Vui lòng kiểm tra hộp thư và làm theo hướng dẫn',
  OK: 'OK',
  'Email không đúng định dạng': 'Email không đúng định dạng',
  'Vui lòng nhập Captcha': 'Vui lòng nhập Captcha',
  'Vui lòng nhập địa chỉ email đã đăng ký': 'Vui lòng nhập địa chỉ email đã đăng ký',
  'Lấy lại mật khẩu': 'Lấy lại mật khẩu',
  'Hủy bỏ': 'Hủy bỏ',
  'Danh mục': 'Ví',
  'Game ID': 'Game ID',
  'Tên Game': 'Tên Game',
  'Số tiền': 'Số tiền',
  'Kết quả': 'Kết quả',
  'Trạng thái': 'Trạng thái',
  STT: '#',
  'Tiền cược': 'Tiền cược',
  'Tiền thắng/thua': 'Tiền thắng/thua',
  'Ngày/Giờ': 'Ngày/Giờ',
  'Transaction ID/Time': 'Mã giao dịch/Thời gian',
  'Ammount/Status': 'Tổng/Trạng thái',
  'Mã giao dịch': 'Mã giao dịch',
  'Nạp tiền': 'Nạp tiền',
  'LỊCH SỬ GIAO DỊCH': 'LỊCH SỬ GIAO DỊCH',
  'Casino trực tuyến': 'Casino trực tuyến',
  'Lịch sử cược': 'Lịch Sử Cược',
  'Casino IM': 'Casino IM',
  'Tất cả': 'Tất cả',
  'Chi tiết cược': 'Chi tiết cược',
  'Tóm tắt': 'Tóm tắt',
  'Loại giao dịch': 'Loại giao dịch',
  Sum: 'Cộng',
  Sub: 'Trừ',
  'Chuyển tiền': 'Chuyển Tiền',
  from: 'Từ',
  to: 'Đến',
  'Thể thao ảo': 'Thể thao ảo',
  'Thông tin cá nhân': 'Thông tin cá nhân',
  'Họ và tên': 'Họ và tên',
  'Ngày sinh': 'Ngày sinh',
  'Giới tính': 'Giới tính',
  'Quốc tịch': 'Quốc tịch',
  'Ví điện tử yêu thích': 'Ví yêu thích',
  CMND: 'CMND',
  'Đơn vị tiền tệ': 'Đơn vị tiền tệ',
  'Ngôn ngữ': 'Ngôn ngữ',
  'Thông tin không thể thay đổi, vui lòng liên hệ chăm sóc khách hàng nếu có gì thắc mắc': 'Thông tin không thể thay đổi, vui lòng liên hệ chăm sóc khách hàng nếu có gì thắc mắc',
  'Số điện thoại': 'Số điện thoại',
  'Địa chỉ': 'Địa chỉ',
  'Địa chỉ Email': 'Địa chỉ Email',
  'Thông tin không thể thay đổi': 'Thông tin không thể thay đổi',
  'Mật khẩu': 'Mật khẩu',
  'Vui lòng nhập đầy đủ họ tên': 'Vui lòng nhập đầy đủ họ tên',
  'Vui lòng nhập ngày sinh': 'Vui lòng nhập ngày sinh',
  'Vui lòng chọn giới tính': 'Vui lòng chọn giới tính',
  'Nhập số điện thoại': 'Nhập số điện thoại',
  'Vui lòng nhập số điện thoại': 'Vui lòng nhập số điện thoại',
  'Nhập số CMND': 'Nhập số CMND',
  'Vui lòng nhập số CMND': 'Vui lòng nhập số CMND',
  'Chọn quốc tịch của bạn': 'Chọn quốc tịch của bạn',
  'Vui lòng chọn quốc tịch': 'Vui lòng chọn quốc tịch',
  'Nhập địa chỉ': 'Nhập địa chỉ',
  'Vui lòng nhập địa chỉ': 'Vui lòng nhập địa chỉ',
  'Chọn loại tiền tệ': 'Chọn loại tiền tệ',
  'Vui lòng chọn đơn vị tiền tệ': 'Vui lòng chọn đơn vị tiền tệ',
  'Chọn ngôn ngữ': 'Chọn ngôn ngữ',
  'Vui lòng chọn ngôn ngữ': 'Vui lòng chọn ngôn ngữ',
  'Chọn ví': 'Chọn ví',
  'Ví trò chơi yêu thích': 'Ví trò chơi yêu thích',
  'Vui lòng chọn ví': 'Vui lòng chọn ví',
  'Thông tin cơ bản': 'Thông tin cơ bản',
  'Thông tin chi tiết': 'Thông tin chi tiết',
  'Cập nhật thông tin': 'Cập nhật thông tin',
  'Hoàn tất': 'Hoàn tất',
  'Xác nhận mật khẩu không chính xác': 'Xác nhận mật khẩu không chính xác',
  'Mật khẩu phải có độ dài 6 ký tự trở lên': 'Mật khẩu phải có độ dài 6 ký tự trở lên',
  'Xác nhận mật khẩu mới': 'Xác nhận mật khẩu mới',
  'Mã OTP': 'Mã OTP',
  'Thay đổi địa chỉ': 'Thay đổi địa chỉ',
  'Vui lòng điền mã OTP đã nhận được': 'Vui lòng điền mã OTP đã nhận được',
  'Tạo mật khẩu mới': 'Tạo mật khẩu mới',
  'Xác nhận địa chỉ Email sai': 'Xác nhận địa chỉ Email sai',
  'Xác nhận mật khẩu sai': 'Xác nhận mật khẩu sai',
  'Vui lòng nhập đúng định dạng email': 'Vui lòng nhập đúng định dạng email',
  'Nhập lại địa chỉ Email': 'Nhập lại địa chỉ Email',
  'Xác nhận lại mật khẩu': 'Xác nhận lại mật khẩu',
  'Vui lòng nhập số điện thoại hợp lệ': 'Vui lòng nhập số điện thoại hợp lệ',
  'Mã đại lý': 'Mã đại lý',
  'Đăng kí mới': 'Đăng kí mới',
  'Tôi hiểu và chấp nhận các chính sách của z88': 'Tôi hiểu và chấp nhận các chính sách của z88',
  'Tôi xác nhận mình đủ 18 tuổi': 'Tôi xác nhận mình đủ 18 tuổi',
  'Đăng ký': 'Đăng ký',
  'Xác nhận địa chỉ email': 'Xác nhận địa chỉ email',
  'Kiểm tra hộp thư của bạn và làm theo hướng dẫn': 'Kiểm tra hộp thư của bạn và làm theo hướng dẫn',
  'trong email để xác nhận tài khoản': 'trong email để xác nhận tài khoản',
  'Xác nhận': 'Xác nhận',
  'Chúc mừng bạn đã xác nhận địa chỉ email thành công': 'Chúc mừng bạn đã xác nhận địa chỉ email thành công',
  'Hãy vào sòng và bắt đầu canh bạc của đời mình': 'Hãy vào sòng và bắt đầu canh bạc của đời mình',
  'Số tiền nạp': 'Số tiền nạp',
  'Nhập số tiền muốn nạp': 'Nhập số tiền muốn nạp',
  'Vui lòng nhập số tiền lớn hơn': 'Vui lòng nhập số tiền lớn hơn',
  'Chúc mừng bạn thanh toán thành công': 'Chúc mừng bạn thanh toán thành công',
  'Không thể chuyển tiền cùng một ví': 'Không thể chuyển tiền cùng một ví',
  'Chọn tài khoản': 'Chọn tài khoản',
  'Đến tài khoản': 'Đến tài khoản',
  'Vui lòng chọn tài khoản nhận': 'Vui lòng chọn tài khoản nhận',
  'Số tiền chuyển': 'Số tiền chuyển',
  'chọn loại tiền tệ bạn muốn sử dụng:': 'Which crypto you want:',
  'Nhập số tiền muốn chuyển': 'Nhập số tiền muốn chuyển',
  'Số tiền phải lớn hơn 0 USD': 'Số tiền phải lớn hơn 0 USD',
  'Số tiền phải chia hết cho 1000 VND': 'Số tiền phải chia hết cho 1000 VND',
  'Chuyển khoản số tiền cược tối thiểu vào tài khoản bạn muốn đặt cược và bắt đầu chơi': 'Chuyển khoản số tiền cược tối thiểu vào tài khoản bạn muốn đặt cược và bắt đầu chơi',
  'Lệnh rút tiền chỉ được thực hiện từ Tài Khoản Chính': 'Lệnh rút tiền chỉ được thực hiện từ Tài Khoản Chính',
  'Chuyển đổi': 'Chuyển đổi',
  'Vui lòng chọn tài khoản chuyển': 'Vui lòng chọn tài khoản chuyển',
  'Từ tài khoản': 'Từ tài khoản',
  'Số tiền rút': 'Số tiền rút',
  '(Tỉ giá hiện tại : 22,134 VNĐ = 1 USD)': '(Tỉ giá hiện tại : 22,134 VNĐ = 1 USD)',
  'Nhập số tiền muốn rút': 'Nhập số tiền muốn rút',
  'Vui lòng nhập số tiền lớn hơn 10 USD': 'Vui lòng nhập số tiền lớn hơn 10 USD',
  'Tài khoản Eknut': 'Tài khoản Eknut',
  'Đăng ký ngay': 'Đăng ký ngay',
  'Chưa có tài khoản': 'Chưa có tài khoản',
  'Email tài khoản eKnut': 'Email tài khoản eKnut',
  'Vui lòng nhập Email tài khoản eKnut': 'Vui lòng nhập Email tài khoản eKnut',
  'Mật khẩu Z88': 'Mật khẩu Z88',
  'Nhập mật khẩu Z88': 'Nhập mật khẩu Z88',
  'Vui lòng nhập mật khẩu Z88': 'Vui lòng nhập mật khẩu Z88',
  'Tìm hiểu thêm': 'Tìm hiểu thêm',
  'Mật khẩu phải nhiều hơn 6 kí tự': 'Mật khẩu phải nhiều hơn 6 kí tự',
  'Email không hợp lệ': 'Email không hợp lệ',
  'Đăng nhập': 'Đăng nhập',
  'ĐĂNG NHẬP': 'ĐĂNG NHẬP',
  'ĐĂNG KÝ': 'ĐĂNG KÝ',
  'Thông tin game': 'Thông tin game',
  'Chơi ngay': 'Chơi ngay',
  'Chơi thử': 'Chơi thử',
  'Cài đặt': 'Cài đặt',
  'CÀI ĐẶT': 'CÀI ĐẶT',
  'Sao kê chi tiết': 'Sao kê chi tiết',
  'Lịch sử giao dịch': 'Lịch Sử Giao Dịch',
  'Game cá': 'Game cá',
  'Tổng dư': 'Tổng Dư',
  'Hôm nay': 'Hôm nay',
  '7 Ngày trước': '7 Ngày trước',
  filter: 'Lọc',
  'BẢNG TIN z88': 'BẢN TIN Z88',
  'Thông báo': 'Thông báo',
  'Vui lòng kiểm tra email và kích hoạt tài khoản.': 'Vui lòng kiểm tra email và kích hoạt tài khoản.',
  // dich sot
  'Hiện hệ thống chỉ hỗ trợ rút tiền qua hệ thống EKnut': 'Hiện hệ thống chỉ hỗ trợ rút tiền qua hệ thống EKnut',
  'Tài khoản': 'Tài khoản',
  'Số dư': 'Số dư',
  'Tài khoản Chính': 'Tài khoản Chính',
  'Đồng ý': 'Đồng ý',
  'Thay đổi': 'Thay đổi',
  'Lần cuối cùng cập nhật': 'Lần cuối cùng cập nhật',
  'Họ Tên': 'Họ Tên',
  '/assets/img/quick-register.jpg': '/assets/img/quick-register.jpg',
  './top-banner.jpg': './top-banner.jpg',
  '/assets/img/promotion-vi.jpg': '/assets/img/promotion-vi.jpg',
  '/assets/img/new-games.jpg': '/assets/img/new-games.jpg',
  '/assets/img/sportbet-vi.jpg': '/assets/img/sportbet-vi.jpg',
  '/assets/img/co-text.png': '/assets/img/co-text.png',

  'VIP Player': 'Dân "VIP"',
  'Cools Playing': 'Chơi phải ngầu',
  'Im VIP': 'Tôi là VIP !',
  'new members': 'Thành viên Mới',
  'ĐANG XÂY DỰNG': 'ĐANG XÂY DỰNG',
  'Tặng 100% thẻ nạp cho lần đầu tiên': 'Tặng 100% thẻ nạp cho lần đầu tiên',
  '/assets/img/text-ban-ca-vi.png': '/assets/img/text-ban-ca-vi.png',
  '/assets/img/text-banner.png': '/assets/img/text-banner.png',
  'Bảng đua top': 'Bảng đua top',
  'Thay đổi số điện thoại': 'Thay đổi số điện thoại',
  'Email lấy lại mật khẩu đã được gửi đến hồm thư của bạn': 'Email lấy lại mật khẩu đã được gửi đến hồm thư của bạn',
  'Phục hồi mật khẩu thành công': 'Phục hồi mật khẩu thành công',
  // tiep translate
  'Bạn chưa xác thực tài khoản email': 'Bạn chưa xác thực tài khoản email',
  'Vui lòng xác thực tài khoản email rồi thực hiện rút tiền': 'Vui lòng xác thực tài khoản email rồi thực hiện rút tiền',
  'Gửi lại email': 'Gửi lại email',
  'Xác nhận thành công': 'Xác nhận thành công',
  'Xác nhận email thất bại': 'Xác nhận email thất bại',
  'Vui lòng liên hệ chăm sóc khách hàng để được hướng dẫn': 'Vui lòng liên hệ chăm sóc khách hàng để được hướng dẫn',
  'Về trang chủ': 'Về trang chủ',
  'Tìm kiếm': 'Tìm kiếm',
  'THÔNG TIN': 'THÔNG TIN',
  'CHƠI NGAY': 'CHƠI NGAY',
  'CHƠI THỬ': 'CHƠI THỬ',
  'Sử dụng': 'Sử dụng',
  'Hoặc ': 'Hoặc ',
  'THÔNG TIN TÀI KHOẢN': 'THÔNG TIN TÀI KHOẢN',
  'Tiếng Việt': 'Tiếng Việt',
  'Tiếng Anh': 'English',
  'VIET NAM': 'Việt Nam',
  'Bạn phải đăng nhập để mở đường dẫn': 'Bạn phải đăng nhập để mở đường dẫn',
  'Hỗ trợ trung tâm': 'Trung Tâm Hỗ Trợ',
  'Hỗ trợ': 'Hỗ trợ',
  'Tin tức': 'Tin tức',
  'Gửi BTC': 'Gửi BTC',
  '/assets/img/live-casino-vi.jpg': '/assets/img/live-casino-vi.jpg',
  '/assets/img/casinoim-vi.jpg': '/assets/img/casinoim-vi.jpg',
  '/assets/img/slots-banner-vi.jpg': '/assets/img/slots-banner-vi.jpg',
  'Số dư hiện tại': 'Số dư hiện tại',
  'Xác thực OTP': 'Xác thực OTP',
  'Giao dịch': 'Giao Dịch',
  'Ví chính': 'Ví Chính',
  'Game number': 'Xổ số',
  'Vui lòng nhập OTP cấp 2 đã gửi qua email của bạn': 'Vui lòng nhập OTP cấp 2 đã gửi qua email của bạn',
  'Nhập những gì bạn muốn tìm': 'Nhập những gì bạn muốn tìm',
  'Tải thêm': 'Tải thêm...',
  'chưa xác định': 'chưa xác định',
  'OTP cấp 2': 'OTP cấp 2',
  'Incorrect captcha.': 'Captcha không đúng.'
}
