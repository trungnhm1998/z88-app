export default {
  'key 1': 'content 1',
  'fishing-content1': '<span style="color: #FD964F">BẮN CÁ TRÚNG THƯỞNG</span> - Trò chơi giải trí rất phổ biến trong các siêu thị, khu vui chơi, trung tâm mua sắm,... bạn cần nạp su để có thể tham gia trò chơi. <span style="color: #FD964F">Thì tại Z88</span> bạn có thể hoàn toàn được <span style="color: #FD964F">chơi bán Cá đổi thưởng miễn phí.</span> không chỉ chơi trong laptop hay máy tính bàn, mà bạn có thể chơi game Bắn Cá ngay trên chiếu điện thoại của mình.',
  'fishing-content2': 'Hãy nhanh nhanh tham gia chơi game trên Z88 để có những giây phút giải trí đầy thú vị cùng với game <span style="color: #FD964F">BẮN CÁ ĐỔI THƯỞNG</span> nhé !',
  'fishing-content3': '<span style="color: #FD964F">Z88</span> sẽ thường xuyên <span style="color: #FD964F">cập nhật game 24h</span> mới nhất, cho các bạn có thể thỏa thích chơi game, thỏa thích khám phá.'
}
