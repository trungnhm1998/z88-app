import _ from 'lodash'
import html from './en.html.locale'
import ens from './en.locale'

const en = _.assign(html, ens)
export { en }
