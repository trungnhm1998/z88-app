import { all, fork } from 'redux-saga/effects'
import * as z88 from './z88'

function* rootSagas() {
  yield all([
    fork(z88.changePassword),
    fork(z88.doDepositTransaction),
    fork(z88.doTransferTransaction),
    fork(z88.getCurrentBalanceStatus),
    fork(z88.getDepositTransactionResult),
    fork(z88.getHistory),
    fork(z88.getInformationOfJackpot),
    fork(z88.getLogToLaunchGame),
    fork(z88.launchTryItGame),
    fork(z88.getPaymentMethods),
    fork(z88.getProfileOfUser),
    fork(z88.getTransferTransactionStatus),
    fork(z88.login),
    fork(z88.logout),
    fork(z88.registerAccountByEmail),
    fork(z88.resetPasswordForgot),
    fork(z88.sendEmailToResetPasswordForgot),
    fork(z88.updateAvatarOfUser),
    fork(z88.updateProfileOfUser),
    fork(z88.getMenuGame),
    fork(z88.checkAccessToken),
    fork(z88.getProductWallet),
    fork(z88.getCaptcha),
    fork(z88.doWithdrawTransaction),
    fork(z88.getTransferHistory),
    fork(z88.getGameLogs),
    fork(z88.getCategoriesGame),
    fork(z88.getGameByCategory),
    fork(z88.getGameCasinoOnline),
    fork(z88.getInviteTop20),
    fork(z88.getCountry),
    fork(z88.getCategoryPromotion),
    fork(z88.getByCategoryPromotion),
    fork(z88.gameGetById),
    fork(z88.sendOtpVerifyEmail),
    fork(z88.sendAuthenOtp),
    fork(z88.getPageOfAlias),
    fork(z88.getBTCPaymentInfo)

  ])
}
export default rootSagas
