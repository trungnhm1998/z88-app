// import _ from 'lodash'
import { takeLatest } from 'redux-saga/effects'
import { types } from 'redux/api/'

export default function* watchAction() {
  yield takeLatest(types.INIT_APPLICATION)
}
