import { call, put, takeLatest } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types } from '../../redux/z88'
import _ from 'lodash'
import { types as apiTypes } from '../../redux/api'
import { z88API } from '../../api'

function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, data } = yield call(z88API.login, action.payload)
    switch (code) {
      case responseCode.LOGIN_SUCCESS: {
        // yield put({ type: 'CHECK_LOGIN' })
        const profile = yield call(z88API.getProfileOfUser, {}, data.accessToken)
        const balanceStatus = yield call(z88API.getCurrentBalanceStatus, {}, data.accessToken)
        const payload = { authenticate: data, userInfo: {}, balanceInfo: {} }
        if (profile.code === responseCode.GET_PROFILE_OF_USER_SUCCESS) {
          payload.userInfo = profile.data
        }
        if (
          balanceStatus.code === responseCode.GET_CURRENT_BALANCE_STATUS_SUCCESS) {
          payload.balanceInfo = balanceStatus.data
        }
        if (!_.isEmpty(payload.userInfo)) {
          yield put({
            type: types.LOGIN_SUCCESS,
            payload
          })
        } else {
          yield put({ type: types.HANDLE_ERROR, payload: code })
          yield put({ type: types.EMIT_MESSAGE, payload: data })
        }

        yield put({ type: types.GET_LOG_TO_LAUNCH_GAME, payload: { gameId: 1 } })
        yield put({ type: types.GET_LOG_TO_LAUNCH_GAME, payload: { gameId: 2 } })
        
        break
      }
      default: {
        if (data.captcha) {
          yield put({ type: types.GET_CAPTCHA_SUCCESS, payload: { imgCaptcha: data.captcha } })
        } else {
          yield put({ type: types.HANDLE_ERROR, payload: code })
          yield put({ type: types.EMIT_MESSAGE, payload: data })
        }
      }
    }
    yield put({ type: apiTypes.HIDE_LOADING })
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put({ type: types.EMIT_MESSAGE, payload: { message: error } })
  }
}


export default function* watchAction() {
  yield takeLatest(types.LOGIN, doAction)
}
