import { call, put, takeLatest } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types } from '../../redux/z88'
import { types as apiTypes } from '../../redux/api'
import { z88API } from '../../api'


function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, data } = yield call(z88API.getProfileOfUser, action.payload)
    yield put({ type: apiTypes.HIDE_LOADING })
    switch (code) {
      case responseCode.GET_PROFILE_OF_USER_SUCCESS: {
        yield put({ type: types.CHECK_ACCESS_TOKEN_SUCCESS })
        yield put({ type: types.GET_PROFILE_OF_USER_SUCCESS, payload: data })
        break
      }

      default: {
        yield put({ type: types.CHECK_ACCESS_TOKEN_FAIL })
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put({ type: types.CHECK_ACCESS_TOKEN_FAIL })
  }
}

export default function* watchAction() {
  yield takeLatest(types.CHECK_ACCESS_TOKEN, doAction)
}
