import { call, put, takeLatest } from 'redux-saga/effects'
// import * as responseCode from '../../configs/responseCode.config'
import { types } from '../../redux/z88'
import { types as apiTypes } from '../../redux/api'
import { z88API } from '../../api'


function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, data } = yield call(z88API.getByCategoryPromotion, action.payload)
    yield put({ type: apiTypes.HIDE_LOADING })
    switch (code) {
      case 50100: {
        yield put({ type: types.GET_BY_CATEGORY_PROMOTION_SUCCESS, payload: { ...data, categoryId: action.payload.categoryId, page: action.payload.page } })
        break
      }

      default: {
        yield put({ type: types.GET_BY_CATEGORY_PROMOTION_FAIL, payload: {} })
        yield put({ type: types.HANDLE_ERROR, payload: code })
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put({ type: types.EMIT_MESSAGE, payload: { message: error } })
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_BY_CATEGORY_PROMOTION, doAction)
}
