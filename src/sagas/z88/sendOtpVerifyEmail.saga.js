import { call, put, takeLatest } from 'redux-saga/effects'
import { types } from '../../redux/z88'
import { types as apiTypes } from '../../redux/api'
import { z88API } from '../../api'


function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, data } = yield call(z88API.sendOtpVerifyEmail, action.payload)
    yield put({ type: apiTypes.HIDE_LOADING })
    switch (code) {
      case 61000: {
        yield put({ type: types.SEND_OTP_VERIFY_EMAIL_SUCCESS, payload: data })
        break
      }
      default: {
        yield put({ type: types.SEND_OTP_VERIFY_EMAIL_FAIL, payload: data })
        yield put({ type: types.EMIT_MESSAGE, payload: { message: data.message } })
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put({ type: types.EMIT_MESSAGE, payload: { message: error } })
  }
}

export default function* watchAction() {
  yield takeLatest(types.SEND_OTP_VERIFY_EMAIL, doAction)
}
