import { call, put, takeLatest } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types } from '../../redux/z88'
import { z88API } from '../../api'


function* doAction(action) {
  try {
    const { code, data } = yield call(z88API.getCaptcha, action.payload)
    switch (code) {
      case responseCode.GET_CAPTCHA_SUCCESS: {
        yield put({ type: types.GET_CAPTCHA_SUCCESS, payload: data })
        break
      }

      default: {
        yield put({ type: types.GET_CAPTCHA_FAIL, payload: data })
      }
    }
  } catch (error) {
    yield put({ type: types.GET_CAPTCHA_FAIL, payload: { message: error } })
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_CAPTCHA, doAction)
}
