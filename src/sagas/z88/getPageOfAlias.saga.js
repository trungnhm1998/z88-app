import { call, put, takeLatest } from 'redux-saga/effects'
import { types } from '../../redux/z88'
import { types as apiTypes } from '../../redux/api'
import { z88API } from '../../api'


function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })

    const { alias, lang } = action.payload
    const { code, data } = yield call(z88API.getPageOfAlias, { alias, lang })
    if (code === 80000) {
      yield put({ type: types.GET_PAGE_OF_ALIAS_SUCCESS, payload: data })
    } else {
      yield put({ type: types.GET_PAGE_OF_ALIAS_FAIL, payload: {} })
    }
    yield put({ type: apiTypes.HIDE_LOADING })
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put({ type: types.EMIT_MESSAGE, payload: { message: error } })
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_PAGE_OF_ALIAS, doAction)
}
