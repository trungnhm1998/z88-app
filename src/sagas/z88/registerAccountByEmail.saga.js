import { call, put, takeLatest } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'

import { types } from '../../redux/z88'
import { types as apiTypes } from '../../redux/api'
import { z88API } from '../../api'


function* doAction(action) {
  try {
    const { email, password, captcha, ...props } = action.payload
    const profileInfo = { ...props }
    const registerInfo = { email, password, captcha }
    if (profileInfo.invite) {
      registerInfo.invite = profileInfo.invite
      delete profileInfo.invite
    }
    if (profileInfo.channel) {
      delete profileInfo.channel
    }
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code: responseCodeRegister, data: responseDataRegister } = yield call(z88API.registerAccountByEmail, registerInfo)
    switch (responseCodeRegister) {
      case responseCode.REGISTER_ACCOUNT_BY_EMAIL_SUCCESS: {
        const { code } = yield call(z88API.updateProfileOfUser, profileInfo, responseDataRegister.accessToken)
        if (code === responseCode.UPDATE_PROFILE_OF_USER_SUCCESS) {
          yield put({ type: types.UPDATE_PROFILE_OF_USER_SUCCESS, payload: { ...action.payload, ...props } })
          const rsGetProfile = yield call(z88API.getProfileOfUser, profileInfo, responseDataRegister.accessToken)
          if (rsGetProfile.code === responseCode.GET_PROFILE_OF_USER_SUCCESS) {
            yield put({ type: types.GET_PROFILE_OF_USER_SUCCESS, payload: rsGetProfile.data })
          }
        }
        yield put({ type: types.REGISTER_ACCOUNT_BY_EMAIL_SUCCESS, payload: responseDataRegister })
        break
      }
      default: {
        yield put({ type: types.HANDLE_ERROR, payload: responseCodeRegister })
        yield put({ type: types.EMIT_MESSAGE, payload: responseDataRegister })
        yield put({ type: types.GET_CAPTCHA })
      }
    }
    yield put({ type: apiTypes.HIDE_LOADING })
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put({ type: types.EMIT_MESSAGE, payload: { message: error } })
  }
}

export default function* watchAction() {
  yield takeLatest(types.REGISTER_ACCOUNT_BY_EMAIL, doAction)
}
