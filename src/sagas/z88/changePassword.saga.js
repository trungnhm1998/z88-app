import { call, put, takeLatest } from 'redux-saga/effects'
import { z88API } from '../../api'
import * as responseCode from '../../configs/responseCode.config'
import { types } from '../../redux/z88'
import { types as apiTypes } from '../../redux/api'


function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, data } = yield call(z88API.changePassword, action.payload)
    yield put({ type: apiTypes.HIDE_LOADING })
    switch (code) {
      case responseCode.CHANGE_PASSWORD_SUCCESS: {
        yield put({ type: types.CHANGE_PASSWORD_SUCCESS, payload: data })
        break
      }

      default: {
        yield put({ type: types.HANDLE_ERROR, payload: code })
        yield put({ type: types.EMIT_MESSAGE, payload: { ...data, type: types.CHANGE_PASSWORD_API } })
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put({ type: types.EMIT_MESSAGE, payload: { message: error } })
  }
}

export default function* watchAction() {
  yield takeLatest(types.CHANGE_PASSWORD, doAction)
}
