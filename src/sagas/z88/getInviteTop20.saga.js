import { call, put, takeLatest } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types } from '../../redux/z88'
import { types as apiTypes } from '../../redux/api'
import { z88API } from '../../api'


function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, data } = yield call(z88API.getInviteTop20, action.payload)
    switch (code) {
      case responseCode.GET_INVITE_TOP_20_SUCCESS: {
        yield put({ type: types.GET_INVITE_TOP_20_SUCCESS, payload: data })
        break
      }
      default: {
        yield put({ type: types.GET_INVITE_TOP_20_FAIL, payload: {} })
      }
    }
    yield put({ type: apiTypes.HIDE_LOADING })
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put({ type: types.EMIT_MESSAGE, payload: { message: error } })
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_INVITE_TOP_20, doAction)
}
