import { call, put, takeLatest } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types } from '../../redux/z88'
import { types as apiTypes } from '../../redux/api'
import { z88API } from '../../api'


function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, data } = yield call(z88API.getCategoriesGame, action.payload)
    switch (code) {
      case responseCode.GET_CATEGORIES_GAME_SUCCESS: {
        const res = []
        const categories = data.categoryList
        let flag = false
        for (let i = 0; i < categories.length; i++) {
          if (categories[i].hasChild > 0) {
            flag = true
            for (let j = 0; j < categories[i].children.length; j++) {
              const rsGame = yield call(z88API.getGameByCategory, { category: categories[i].children[j].categoryId, platform: 'mobile' })
              if (rsGame.code === responseCode.GET_GAME_BY_CATEGORY_SUCCESS) {
                res.push({
                  productWallet: action.payload.productWallet, categoryId: categories[i].categoryId, category: categories[i].children[j].categoryId, data: rsGame.data.games 
                })
              }
            }
            break
          } else if (flag === false) {
            const rsGame = yield call(z88API.getGameByCategory, { category: categories[i].categoryId, platform: 'mobile' })
            if (rsGame.code === responseCode.GET_GAME_BY_CATEGORY_SUCCESS) {
              res.push({
                productWallet: action.payload.productWallet, categoryId: -1, category: categories[i].categoryId, data: rsGame.data.games 
              })
            }
          }
        }
        yield put({ type: types.GET_CATEGORIES_GAME_SUCCESS, payload: { [action.payload.productWallet]: data.categoryList ? data.categoryList : [] } })
        yield put({ type: types.GET_GAME_BY_CATEGORY_SUCCESS, payload: res })
        yield put({ type: types.EMIT_MESSAGE, payload: '' })
        break
      }
      default: {
        yield put({ type: types.HANDLE_ERROR, payload: code })
        yield put({ type: types.EMIT_MESSAGE, payload: data })
      }
    }
    yield put({ type: apiTypes.HIDE_LOADING })
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put({ type: types.EMIT_MESSAGE, payload: { message: error } })
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_CATEGORIES_GAME, doAction)
}
