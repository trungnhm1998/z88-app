import { call, put, takeLatest } from 'redux-saga/effects'
// import * as responseCode from '../../configs/responseCode.config'
import { types } from '../../redux/z88'
import { types as apiTypes } from '../../redux/api'
import { z88API } from '../../api'


function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, data } = yield call(z88API.getCategoryPromotion)
    yield put({ type: apiTypes.HIDE_LOADING })
    switch (code) {
      case 50000: {
        yield put({ type: types.GET_CATEGORY_PROMOTION_SUCCESS, payload: data.categoryList })
        const categoryId = (action.payload.categoryId === 0) ? data.categoryList[0].id : action.payload.categoryId
        const rsApiPromotion = yield call(z88API.getByCategoryPromotion, { categoryId, page: action.payload.page })
        if (rsApiPromotion.code === 50100) {
          yield put({ type: types.GET_BY_CATEGORY_PROMOTION_SUCCESS, payload: { ...rsApiPromotion.data, categoryId, page: action.payload.page } })
        } else {
          yield put({ type: types.GET_BY_CATEGORY_PROMOTION_FAIL })
        }
        break
      }

      default: {
        yield put({ type: types.GET_CATEGORY_PROMOTION_FAIL, payload: {} })
        yield put({ type: types.HANDLE_ERROR, payload: code })
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put({ type: types.EMIT_MESSAGE, payload: { message: error } })
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_CATEGORY_PROMOTION, doAction)
}
