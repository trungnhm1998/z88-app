import { call, apply, put, takeLatest } from 'redux-saga/effects'
import { types } from '../../redux/z88'
import { types as apiTypes } from '../../redux/api'
import { z88API } from '../../api'


function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    console.log('request')
    const { code, data } = yield call(z88API.sendAuthenOtp, action.payload)
    yield put({ type: apiTypes.HIDE_LOADING })
    switch (code) {
      case 19000: {
        yield put({ type: types.SEND_AUTHEN_OTP_SUCCESS, payload: data })
        console.log('scucess')
        break
      }
      default: {
        yield put({ type: types.SEND_AUTHEN_OTP_FAIL, payload: data })
        yield put({ type: types.EMIT_MESSAGE, payload: { message: data.message } })
        console.log('fail')
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put({ type: types.EMIT_MESSAGE, payload: { message: error } })
    console.log(`error ${error}`)
  }
}

export default function* watchAction() {
  yield takeLatest(types.SEND_AUTHEN_OTP, doAction)
}
