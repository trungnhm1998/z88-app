import { call, put, takeLatest } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types } from '../../redux/z88'

import { types as apiTypes } from '../../redux/api'
import { z88API } from '../../api'


function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, data } = yield call(z88API.doDepositTransaction, action.payload)
    switch (code) {
      case responseCode.DO_DEPOSIT_TRANSACTION_SUCCESS: {
        yield call(z88API.getCurrentBalanceStatus, {})
        yield put({ type: types.DO_DEPOSIT_TRANSACTION_SUCCESS, payload: data })
        break
      }

      default: {
        yield put({ type: types.HANDLE_ERROR, payload: code })
        yield put({ type: types.EMIT_MESSAGE, payload: data })
      }
    }
    yield put({ type: apiTypes.HIDE_LOADING })
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put({ type: types.EMIT_MESSAGE, payload: { message: error } })
  }
}

export default function* watchAction() {
  yield takeLatest(types.DO_DEPOSIT_TRANSACTION, doAction)
}
