import { call, put, takeLatest } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types } from '../../redux/z88'

import { types as apiTypes } from '../../redux/api'
import { z88API } from '../../api'


function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, data } = yield call(z88API.resetPasswordForgot, action.payload)
    yield put({ type: apiTypes.HIDE_LOADING })
    switch (code) {
      case responseCode.RESET_PASSWORD_FORGOT_SUCCESS: {
        yield put({ type: types.RESET_PASSWORD_FORGOT_SUCCESS, payload: data })
        break
      }

      default: {
        yield put({ type: types.HANDLE_ERROR, payload: code })
        yield put({ type: types.EMIT_MESSAGE, payload: data })
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put({ type: types.EMIT_MESSAGE, payload: { api: types.RESET_PASSWORD_API, message: error } })
  }
}

export default function* watchAction() {
  yield takeLatest(types.RESET_PASSWORD_FORGOT, doAction)
}
