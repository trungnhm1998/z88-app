import { call, put, takeLatest } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types } from '../../redux/z88'
import { types as apiTypes } from '../../redux/api'
import { z88API } from '../../api'


function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, data } = yield call(z88API.getCurrentBalanceStatus, action.payload)
    switch (code) {
      case responseCode.GET_CURRENT_BALANCE_STATUS_SUCCESS: {
        yield put({ type: types.GET_CURRENT_BALANCE_STATUS_SUCCESS, payload: data })
        break
      }

      default: {
        yield put({ type: types.GET_CURRENT_BALANCE_STATUS_FAIL, payload: data })
      }
    }
    yield put({ type: apiTypes.HIDE_LOADING })
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put({ type: types.GET_CURRENT_BALANCE_STATUS_FAIL })
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_CURRENT_BALANCE_STATUS, doAction)
}
