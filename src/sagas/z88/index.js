import changePassword from './changePassword.saga'
import doDepositTransaction from './doDepositTransaction.saga'
import doTransferTransaction from './doTransferTransaction.saga'
import doWithdrawTransaction from './doWithdrawTransaction.saga'
import getCurrentBalanceStatus from './getCurrentBalanceStatus.saga'
import getDepositTransactionResult from './getDepositTransactionResult.saga'
import getHistory from './getHistory.saga'
import getInformationOfJackpot from './getInformationOfJackpot.saga'
import getLogToLaunchGame from './getLogToLaunchGame.saga'
import launchTryItGame from './launchTryItGame.saga'
import getPaymentMethods from './getPaymentMethods.saga'
import getProfileOfUser from './getProfileOfUser.saga'
import getTransferTransactionStatus from './getTransferTransactionStatus.saga'
import login from './login.saga'
import logout from './logout.saga'
import registerAccountByEmail from './registerAccountByEmail.saga'
import resetPasswordForgot from './resetPasswordForgot.saga'
import sendEmailToResetPasswordForgot from './sendEmailToResetPasswordForgot.saga'
import updateAvatarOfUser from './updateAvatarOfUser.saga'
import updateProfileOfUser from './updateProfileOfUser.saga'
import getMenuGame from './getMenuGame.saga'
import checkAccessToken from './checkAccessToken.saga'
import getProductWallet from './getProductWallet.saga'
import getCaptcha from './getCaptcha.saga'
import getTransferHistory from './getTransferHistory.saga'
import getGameLogs from './getGameLogs.saga'
import getCategoriesGame from './getCategoriesGame.saga'
import getGameByCategory from './getGameByCategory.saga'
import getGameCasinoOnline from './getGameCasinoOnline.saga'
import getInviteTop20 from './getInviteTop20.saga'
import getCountry from './getCountry.saga'
import getCategoryPromotion from './getCategoryPromotion.saga'
import getByCategoryPromotion from './getByCategoryPromotion.saga'
import gameGetById from './gameGetById.saga'
import sendOtpVerifyEmail from './sendOtpVerifyEmail.saga'
import sendAuthenOtp from './sendAuthenOtp.saga'
import getPageOfAlias from './getPageOfAlias.saga'
import getBTCPaymentInfo from './getBTCPaymentInfo.saga'

export {
  changePassword,
  doDepositTransaction,
  doTransferTransaction,
  doWithdrawTransaction,
  getCurrentBalanceStatus,
  getDepositTransactionResult,
  getHistory,
  getInformationOfJackpot,
  getLogToLaunchGame,
  launchTryItGame,
  getPaymentMethods,
  getProfileOfUser,
  getTransferTransactionStatus,
  login,
  logout,
  registerAccountByEmail,
  resetPasswordForgot,
  sendEmailToResetPasswordForgot,
  updateAvatarOfUser,
  updateProfileOfUser,
  getMenuGame,
  checkAccessToken,
  getProductWallet,
  getCaptcha,
  getTransferHistory,
  getGameLogs,
  getCategoriesGame,
  getGameByCategory,
  getGameCasinoOnline,
  getInviteTop20,
  getCountry,
  getCategoryPromotion,
  getByCategoryPromotion,
  gameGetById,
  sendOtpVerifyEmail,
  sendAuthenOtp,
  getPageOfAlias,
  getBTCPaymentInfo
}
