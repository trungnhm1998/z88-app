import { call, put, takeLatest } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types } from '../../redux/z88'
import { types as apiTypes } from '../../redux/api'
import { z88API } from '../../api'


function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })

    const { productWallet, categoryId = -1, categories = [] } = action.payload
    const res = []
    for (let i = 0; i < categories.length; i++) {
      const { code, data } = yield call(z88API.getGameByCategory, { category: categories[i], platform: 'mobile' })
      if (code === responseCode.GET_GAME_BY_CATEGORY_SUCCESS) {
        res.push({ productWallet, categoryId, category: categories[i], data: data.games })
      }
    }
    yield put({ type: types.GET_GAME_BY_CATEGORY_SUCCESS, payload: res })
    yield put({ type: types.EMIT_MESSAGE, payload: '' })
    yield put({ type: apiTypes.HIDE_LOADING })
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put({ type: types.EMIT_MESSAGE, payload: { message: error } })
  }
}

export default function* watchAction() {
  yield takeLatest(types.GET_GAME_BY_CATEGORY, doAction)
}
