import { call, put, takeLatest } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types } from '../../redux/z88'
import { types as apiTypes } from '../../redux/api'
import { z88API } from '../../api'


function* doAction(action) {
  // fixed the pop up blocked by open the blank window first the get the reference of the window the set url later
  // refs: https://yaplex.com/blog/avoid-browser-pop-up-blockers
  let gameWindow
  if (action.payload.gameId >= 3) {
    const windowId = (new Date()).getTime()
    gameWindow = window.open('', windowId, '')
  }

  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, data } = yield call(z88API.LaunchTryItGame, action.payload)
    yield put({ type: apiTypes.HIDE_LOADING })
    switch (code) {
      case responseCode.LAUNCH_TRY_IT_GAME_SUCCESS: {
        yield put({ type: types.LAUNCH_TRY_IT_GAME_SUCCESS, payload: { ...data, gameId: action.payload.gameId, gameWindow } })
        yield put({ type: types.EMIT_MESSAGE, payload: {} })
        break
      }

      default: {
        yield put({ type: types.LAUNCH_TRY_IT_GAME_FAIL, payload: { ...data, gameId: action.payload.gameId, gameWindow } })
        yield put({ type: types.HANDLE_ERROR, payload: code })
        yield put({ type: types.EMIT_MESSAGE, payload: data })
        yield put({ type: types.EMIT_ALERT, payload: { content: data.message, level: 3 } })
      }
    }
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put({ type: types.EMIT_MESSAGE, payload: { message: error } })
  }
}

export default function* watchAction() {
  yield takeLatest(types.LAUNCH_TRY_IT_GAME, doAction)
}
