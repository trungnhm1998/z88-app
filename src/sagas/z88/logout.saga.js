import { call, put, takeLatest } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types } from '../../redux/z88'
import { types as apiTypes } from '../../redux/api'
import { z88API } from '../../api'


function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const { code, data } = yield call(z88API.logout, action.payload)
    switch (code) {
      case responseCode.LOGOUT_SUCCESS: {
        yield put({ type: types.LOGOUT_SUCCESS, payload: data })
        break
      }

      default: {
        yield put({ type: types.LOGOUT_FAIL })
      }
    }
    yield put({ type: apiTypes.HIDE_LOADING })
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put({ type: types.LOGOUT_FAIL })
  }
}

export default function* watchAction() {
  yield takeLatest(types.LOGOUT, doAction)
}
