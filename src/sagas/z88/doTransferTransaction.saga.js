import { call, put, takeLatest } from 'redux-saga/effects'
import * as responseCode from '../../configs/responseCode.config'
import { types } from '../../redux/z88'
import { types as apiTypes } from '../../redux/api'
import { z88API } from '../../api'


function* doAction(action) {
  try {
    yield put({ type: apiTypes.SHOW_LOADING })
    const [firstTransaction, secondTransaction] = action.payload
    const { code: codeFirstTransaction, data: dataFirstTransaction } = yield call(z88API.doTransferTransaction, firstTransaction)
    switch (codeFirstTransaction) {
      case responseCode.DO_TRANSFER_TRANSACTION_SUCCESS: {
        if (secondTransaction) {
          const { code: codeSecondTransaction, data: dataSecondTransaction } = yield call(z88API.doTransferTransaction, secondTransaction)
          if (responseCode.DO_TRANSFER_TRANSACTION_SUCCESS === codeSecondTransaction) {
            yield put({ type: types.DO_TRANSFER_TRANSACTION_SUCCESS, payload: dataSecondTransaction })
            break
          }
        }
        yield put({ type: types.DO_TRANSFER_TRANSACTION_SUCCESS, payload: dataFirstTransaction })
        break
      }
      default: {
        yield put({ type: types.HANDLE_ERROR, payload: codeFirstTransaction })
        yield put({ type: types.EMIT_MESSAGE, payload: dataFirstTransaction })
      }
    }
    yield put({ type: apiTypes.HIDE_LOADING })
  } catch (error) {
    yield put({ type: apiTypes.HIDE_LOADING })
    yield put({ type: types.EMIT_MESSAGE, payload: { message: error } })
  }
}

export default function* watchAction() {
  yield takeLatest(types.DO_TRANSFER_TRANSACTION, doAction)
}
