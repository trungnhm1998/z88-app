export const getAccessToken = state => state.z88.authenticate.accessToken
export const getInfoClient = state => state.z88.infoClient
export const getLanguage = state => state.i18nState.lang
