import React, { Component } from 'react'
import { StatusBar } from 'react-native'
import { Provider, connect } from 'react-redux'
import I18n from 'redux-i18n'
import { createStore, applyMiddleware } from 'redux'
import Router from './configs/router.config'
import { getLanguage } from './selector'
import translations from './translations'
import store from './configs/store.config'
// import Router from './routes/routes'


class App extends Component {
  render() {
    StatusBar.setBarStyle('light-content')
    return (
      <Provider store={store}>
        <I18n translations={translations} initialLang={getLanguage(store.getState())} fallbackLang="en">
          <Router />
        </I18n>
      </Provider>
    )
  }
}

export default App
